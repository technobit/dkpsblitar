<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Navbar_model extends MY_Model {
	function get_banner(){
		$limit = 1;
		$offset = 0;
		$this->db->select("var_img_banner")
					->from($this->m_banner)
					->order_by('int_banner_id', 'DESC')
					->limit($limit, $offset);
		
		return $this->db->get()->row();
	}
}