<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Sidebar_model extends MY_Model {
	function get_list_pengaduan(){
		$limit = 5;
		$offset = 0;
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4 ')
					->where('int_type = 1 ')
					->where('int_status = 1 ')
					->order_by('created_at', 'DESC')
					->limit($limit, $offset);
		
		return $this->db->get()->result();
	}

	function get_list_informasi(){
		$limit = 5;
		$offset = 0;
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4 ')
					->where('int_type = 2 ')
					->where('int_status = 1 ')
					->order_by('created_at', 'DESC')
					->limit($limit, $offset);
		
		return $this->db->get()->result();
	}
}