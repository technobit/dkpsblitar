<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
		
		$filter_start30		= date('Y-m-d 00:00:00', strtotime(date('Y-m-d'). ' -29 days'));
		$filter_start7		= date('Y-m-d 00:00:00', strtotime(date('Y-m-d'). ' -6 days'));
		$filter_start		= date('Y-m-d 00:00:00');
		$filter_end			= date('Y-m-d 23:59:59');

		$data = array();
		$data['adu30'] = $this->model->count(1, $filter_start30, $filter_end);
		$data['adu7'] = $this->model->count(1, $filter_start7, $filter_end);
		$data['adu'] = $this->model->count(1, $filter_start, $filter_end);

		$data['info30'] = $this->model->count(2, $filter_start30, $filter_end);
		$data['info7'] = $this->model->count(2, $filter_start7, $filter_end);
		$data['info'] = $this->model->count(2, $filter_start, $filter_end);

		$this->render_view('index', $data, false,);
	}


}
