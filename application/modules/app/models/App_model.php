<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {
    public function count($type, $filter_start, $filter_end){

        $this->db->select("*")
				->from($this->t_pengaduan)
                ->where('(dt_pengaduan BETWEEN "'.$filter_start.'" AND "'.$filter_end.'")')
				->where("int_type", $type);
		
        return $this->db->count_all_results();
    }
}