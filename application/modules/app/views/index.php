<?//=print_r($data)?>
<div class="container-fluid">
  <div class="row">
    <div class="card card-outline col-12">
      <div class="card-header">
        <h3 class="card-title">Aspirasi Hari Ini</h3>
      </div>
      <div class="card-body row">
        <div class="col-md-4 col-sm-12">
          <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-flag"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Pengaduan</span>
              <span class="info-box-number"><?=$adu?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-12">
          <div class="info-box">
            <span class="info-box-icon bg-primary"><i class="fas fa-info-circle"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Informasi</span>
              <span class="info-box-number"><?=$info?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-4 col-sm-12">
          <div class="info-box">
            <span class="info-box-icon bg-warning"><i class="fas fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Aspirasi</span>
              <span class="info-box-number"><?php echo ($adu + $info)?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <div class="card card-outline col-12">
      <div class="card-header">
        <h3 class="card-title">Aspirasi 1 Minggu Terakhir</h3>
      </div>
      <div class="card-body row">
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=$adu7?></h3>
              <p>Pengaduan</p>
            </div>
            <div class="icon">
              <i class="fas fa-flag"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=$adu7?></h3>
              <p>Informasi</p>
            </div>
            <div class="icon">
              <i class="fas fa-info-circle"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-warning">
            <div class="inner">
              <h3><?php echo ($adu7 + $info7)?></h3>
              <p>Total Aspirasi</p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <div class="card card-outline col-12">
      <div class="card-header">
        <h3 class="card-title">Aspirasi 30 Hari Terakhir</h3>
      </div>
      <div class="card-body row">
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-dark">
            <div class="inner">
              <p style="margin-bottom:0px">Pengaduan</p>
              <h3 class="text-right" style="margin:-20px 80px 0 0;font-size:80px"><?=$adu30?></h3>
            </div>
            <div class="icon">
              <i class="fas fa-flag"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-success">
            <div class="inner">
              <p style="margin-bottom:0px">Informasi</p>
              <h3 class="text-right" style="margin:-20px 80px 0 0;font-size:80px"><?=$adu30?></h3>
            </div>
            <div class="icon">
              <i class="fas fa-info-circle"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <!-- small card -->
          <div class="small-box bg-orange">
            <div class="inner">
              <p style="margin-bottom:0px">Total Aspirasi</p>
              <h3 class="text-right" style="margin:-20px 80px 0 0;font-size:80px"><?php echo ($adu30 + $info30)?></h3>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
  </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>