<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['add_pengaduan']['get']      			    = 'homepage/homepage/add_pengaduan';
$route['add_pengaduan/save']['post']                = 'homepage/homepage/save_pengaduan';

$route['add_informasi']['get']      			    = 'homepage/homepage/add_informasi';
$route['add_informasi/save']['post']                = 'homepage/homepage/save_informasi';

$route['search_form']['get']                = 'homepage/homepage/add_search';
$route['search']['post']      = 'homepage/homepage/search';

$route['info']['get']               = 'homepage/info/index';
$route['info/([0-9]+)']['get']      = 'homepage/info/index/$1';
$route['info/det/([0-9]+)']['get']  = 'homepage/info/detil/$1';

$route['adu']['get']                = 'homepage/adu/index';
$route['adu/([0-9]+)']['get']       = 'homepage/adu/index/$1';
$route['adu/det/([0-9]+)']['get']   = 'homepage/adu/detil/$1';
