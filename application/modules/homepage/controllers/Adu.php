<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adu extends MX_Controller {
	var $limit = "10";
	function __construct(){
        parent::__construct();
		
        $this->module   = 'homepage';
		$this->routeURL = 'adu';

		$this->load->model('adu_model', 'model');
    }

	public function index($page = 1){
		$start = $this->limit * ($page - 1);

		$pengaduan = $this->model->get_adu_list($start, $this->limit);
		$total_adu = $this->model->count_adu_list();
		if(empty($pengaduan)){
			header('Location: '.base_url().'');
			exit;
		}
		
		$list_pengaduan = '';
		foreach($pengaduan as $adu){
			//meta posts
			if($adu->int_progress == 0){
				$progress = '<span class="badge badge-pill bg-dark mb-1">Draft</span>';
			} else if ($adu->int_progress == 1){
				$progress = '<span class="badge badge-pill bg-warning mb-1">Menunggu Persetujuan</span>';
			} else if ($adu->int_progress == 2){
				$progress = '<span class="badge badge-pill bg-danger mb-1">Ditolak</span>';
			} else if ($adu->int_progress == 3){
				$progress = '<span class="badge badge-pill bg-primary mb-1">Diproses</span>';
			} else if ($adu->int_progress == 4){
				$progress = '<span class="badge badge-pill bg-success mb-1">Selesai</span>';
			}
			$this->page->title = $adu->var_topik.' | Pengaduan Masyarakat';
			$list_pengaduan .= '<div>
									<i class="fas fa-comments bg-danger" style="color:#fff"></i>
									<div class="timeline-item">
										<span class="time">
											<i class="fas fa-clock"></i> '.idn_date($adu->created_at, 'l, j F Y H:i:s').'
										</span>
										<h3 class="timeline-header">
											<span class="badge badge-pill bg-danger mb-1">'.$adu->var_kategori.'</span><br>
											<a style="color:#495057" href="'.base_url('adu/det/').$adu->int_pengaduan_id.'">'.$adu->var_topik.'</a>
											'.$progress.'
										</h3>
										<div class="timeline-body">
										'.strip_tags(substr($adu->txt_note,0,300)).'...
										</div>
										<div class="timeline-footer">

										</div>
									</div>
								</div>';

		}
		
		$data['list_pengaduan'] = $list_pengaduan;
		$data["info_paging"] = $this->_build_pagination($page, $total_adu, $this->limit, base_url()."adu/");

		$this->render_view('adu/index', $data, false, 'public_sidebar');
		//$this->render_view('homepage/index', $data, false, 'public');
	}

	public function detil($int_pengaduan_id){

		$res = $this->model->get_adu_detail($int_pengaduan_id);
		
		if(empty($res)){
			$this->render_view(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$adu_respon = $this->model->get_adu_respon($int_pengaduan_id);
			$data['data']		= $res;
			$data['respon']		= $adu_respon;
			//$data['routeURL']	= $this->routeURL;
			//$data['url']		= site_url("{$this->routeURL}/$int_pengaduan_id");
			$this->render_view('adu/detil', $data, false, 'public_sidebar');
		}
	}
}
