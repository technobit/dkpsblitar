<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends MX_Controller {
	private $input_file_name = 'lampiran';
	function __construct(){
        parent::__construct();
		
        //$this->kodeMenu = 'APP-HOME';
        $this->module   = 'homepage';
		$this->routeURL = 'homepage';

		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
	
		$this->load->model('master/kategori_model', 'kategori');
		$this->load->model('master/wilayah_model', 'wilayah');
		$this->load->model('homepage_model', 'model');
    }

	public function index(){
		//$this->page->menu = 'dashboard';
		$this->page->subtitle = 'Beranda';
		$this->css = true;
		$this->js = true;

		$data['url'] = site_url("add_pengaduan");
		$data['url_add_pengaduan'] = site_url("add_pengaduan");
		$data['url_add_informasi'] = site_url("add_informasi");
		$data['url_cek_pengaduan'] = site_url("search_form");

		$pengaduan = $this->model->get_list_pengaduan();
		$informasi = $this->model->get_list_informasi();

		$list_pengaduan = '';
		foreach($pengaduan as $adu){
			$list_pengaduan .= '<div class="card">
									<div class="card-header">
										<span class="badge badge-pill bg-danger mb-1">'.$adu->var_kategori.'</span>
										<a style="color:#495057" href="'.base_url('adu/det/').$adu->int_pengaduan_id.'"><h4>'.$adu->var_topik.'</h4></a>
									</div>
									<div class="card-body">
									<p><span class="fas fa-calendar-alt"> </span> '.idn_date($adu->created_at, 'l, j F Y').'
									<span class="fas fa-clock"> </span> '.idn_date($adu->created_at, 'H:i:s').'</p>
									'.strip_tags(substr($adu->txt_note,0,300)).'...
									</div>
								</div>';
		}

		$list_informasi = '';
		foreach($informasi as $info){
			$list_informasi .= '<div class="card">
									<div class="card-header">
										<span class="badge badge-pill bg-primary mb-1">'.$info->var_kategori.'</span>
										<a style="color:#495057" href="'.base_url('info/det/').$info->int_pengaduan_id.'"><h4>'.$info->var_topik.'</h4></a>
									</div>
									<div class="card-body">
									<p><span class="fas fa-calendar-alt"> </span> '.idn_date($info->created_at, 'l, j F Y').'
									<span class="fas fa-clock"> </span> '.idn_date($info->created_at, 'H:i:s').'</p>
									'.strip_tags(substr($info->txt_note,0,300)).'...
									</div>
								</div>';
		}

		$data['list_pengaduan'] = $list_pengaduan;
		$data['list_informasi'] = $list_informasi;

		$this->render_view('homepage/index', $data, false, 'public');
	}

	public function add_pengaduan(){
		$data['url']        = site_url("add_pengaduan/save");
		$data['title']      = 'Form Pengaduan Masyarakat';
		$data['kategori']	= $this->kategori->get_list('int_pengaduan');
		$data['kelurahan']	= $this->wilayah->get_kelurahan();
		$this->load_view('homepage/form_pengaduan', $data, true);
		//$this->load_view('pengaduan/index_action', $data, true);
	}

	public function save_pengaduan(){	
		
		$this->form_validation->set_rules('var_nik', 'NIK/No. KTP', 'required');
		$this->form_validation->set_rules('var_nama', 'Nama', 'required');
		$this->form_validation->set_rules('var_alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('var_no_hp', 'No. Ponsel', 'required');
		$this->form_validation->set_rules('var_email', 'eMail', 'required|valid_email');
		$this->form_validation->set_rules('int_kategori_id', 'Kategori Pengaduan', 'required');
		$this->form_validation->set_rules('var_topik', 'Detil Pengaduan', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$input_post = $this->input->post();
			$input_post['int_type'] = 1;
			//$this->input->post('int_type') = 
            $insert = $this->model->create_pengaduan($input_post);

			$this->set_json([  'stat' => ($insert)? true : false, 
								'mc' => false,
								'no_tiket' => $insert,
								'nama' => $this->input->post('var_nama'),
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}

	public function add_informasi(){
		$data['url']        = site_url("add_informasi/save");
		$data['title']      = 'Form Informasi Masyarakat';
		$data['kategori']	= $this->kategori->get_list('int_informasi');
		$data['kelurahan']	= $this->wilayah->get_kelurahan();
		$this->load_view('homepage/form_informasi', $data, true);
		//$this->load_view('pengaduan/index_action', $data, true);
	}
	
	public function save_informasi(){	
		
		$this->form_validation->set_rules('var_nik', 'NIK/No. KTP', 'required');
		$this->form_validation->set_rules('var_nama', 'Nama', 'required');
		$this->form_validation->set_rules('var_alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('var_no_hp', 'No. Ponsel', 'required');
		$this->form_validation->set_rules('var_email', 'eMail', 'required|valid_email');
		$this->form_validation->set_rules('int_kategori_id', 'Kategori Informassi', 'required');
		$this->form_validation->set_rules('var_topik', 'Detil Informasi', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {			
			$input_post = $this->input->post();
			$input_post['int_type'] = 2;
			//$this->input->post('int_type') = 
            $insert = $this->model->create_informasi($input_post);

			$this->set_json([  'stat' => ($insert)? true : false, 
								'mc' => false,
								'no_tiket' => $insert,
								'nama' => $this->input->post('var_nama'),
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}

	public function add_search(){
		$data['url']        = site_url("search");
		$data['title']      = 'Cari Pelaporan';
		$this->load_view('homepage/form_search', $data, true);
		//$this->load_view('pengaduan/index_action', $data, true);
	}

	public function search(){
		//$start = $this->limit * ($page - 1);

		$search = $this->model->search_list($this->input->post('keyword'));
		//$total_info = $this->model->count_info_list();
		if(empty($search)){
			header('Location: '.base_url().'');
			exit;
		}
		
		//meta posts
		$this->page->title = 'Pencarian Pelaporan';
		//$this->page->meta_description = $category_posts[0]->txt_desc;
		//$this->page->meta_keywords = $txt_slug.", wisata lumajang, visit lumajang";
		//$this->page->meta_url = base_url().$txt_slug;
		//$this->page->meta_image = cdn_url().'v5/img/vl.png';
		$search_list = '';
		foreach($search as $src){
			if(isset($src->int_informasi_id)){
				$icon = 'fas fa-info bg-primary';
				$badge_color = 'primary';
				$detil_url = base_url('info/det/').$src->int_informasi_id;
			}else if(isset($src->int_pengaduan_id)){
				$icon = 'fas fa-comments bg-danger';
				$badge_color = 'danger';
				$detil_url = base_url('adu/det/').$src->int_pengaduan_id;
			}else{
				$icon = 'fas fa-search bg-success';
				$badge_color = 'success';
				$detil_url = '#';
			}

			if($src->int_progress == 0){
				$progress = '<span class="badge badge-pill bg-black mb-1">Draft</span>';
			} else if ($src->int_progress == 1){
				$progress = '<span class="badge badge-pill bg-warning mb-1">Menunggu Persetujuan</span>';
			} else if ($src->int_progress == 2){
				$progress = '<span class="badge badge-pill bg-danger mb-1">Ditolak</span>';
			} else if ($src->int_progress == 3){
				$progress = '<span class="badge badge-pill bg-primary mb-1">Diproses</span>';
			} else if ($src->int_progress == 4){
				$progress = '<span class="badge badge-pill bg-success mb-1">Selesai</span>';
			}

			$search_list .= '<div>
								<i class="'.$icon.'" style="color:#fff"></i>
								<div class="timeline-item">
									<span class="time">
										<i class="fas fa-clock"></i> '.idn_date($src->dt_pengaduan, 'l, j F Y H:i:s').'
									</span>
									<h3 class="timeline-header">
										<span class="badge badge-pill bg-'.$badge_color.' mb-1">'.$src->var_kategori.'</span><br>
										<a style="color:#495057" href="'.$detil_url.'">'.$src->var_topik.'</a>
										'.$progress.'
										</h3>
									<div class="timeline-body">
									'.strip_tags(substr($src->txt_note,0,300)).'...
									</div>
									<div class="timeline-footer">
									</div>
								</div>
							</div>';

		}
		
		$data['keyword'] = $this->input->post('keyword');
		$data['search_list'] = $search_list;
		//$data["info_paging"] = $this->_build_pagination($page, $total_info, $this->limit, base_url()."info/");

		$this->render_view('homepage/search', $data, false, 'public_sidebar');
		//$this->render_view('homepage/index', $data, false, 'public');
	}
}
