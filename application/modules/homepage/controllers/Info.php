<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends MX_Controller {
	var $limit = "10";
	function __construct(){
        parent::__construct();
		
        $this->module   = 'homepage';
		$this->routeURL = 'info';

		$this->load->model('info_model', 'model');
    }

	public function index($page = 1){
		$start = $this->limit * ($page - 1);

		$informasi = $this->model->get_info_list($start, $this->limit);
		$total_info = $this->model->count_info_list();
		if(empty($informasi)){
			header('Location: '.base_url().'');
			exit;
		}
		
		$list_informasi = '';
		foreach($informasi as $info){
			//meta posts
			if($info->int_progress == 0){
				$progress = '<span class="badge badge-pill bg-dark mb-1">Draft</span>';
			} else if ($info->int_progress == 1){
				$progress = '<span class="badge badge-pill bg-warning mb-1">Menunggu Persetujuan</span>';
			} else if ($info->int_progress == 2){
				$progress = '<span class="badge badge-pill bg-danger mb-1">Ditolak</span>';
			} else if ($info->int_progress == 3){
				$progress = '<span class="badge badge-pill bg-primary mb-1">Diproses</span>';
			} else if ($info->int_progress == 4){
				$progress = '<span class="badge badge-pill bg-success mb-1">Selesai</span>';
			}
			$this->page->title = $info->var_topik.' | Informasi Masyarakat';
			$list_informasi .= '<div>
									<i class="fas fa-info bg-primary" style="color:#fff"></i>
									<div class="timeline-item">
										<span class="time">
											<i class="fas fa-clock"></i> '.idn_date($info->created_at, 'l, j F Y H:i:s').'
										</span>
										<h3 class="timeline-header">
											<span class="badge badge-pill bg-primary mb-1">'.$info->var_kategori.'</span><br>
											<a style="color:#495057" href="'.base_url('info/det/').$info->int_pengaduan_id.'">'.$info->var_topik.'</a>
											'.$progress.'
										</h3>
										<div class="timeline-body">
										'.strip_tags(substr($info->txt_note,0,300)).'...
										</div>
										<div class="timeline-footer">

										</div>
									</div>
								</div>';

		}
		
		$data['list_informasi'] = $list_informasi;
		$data["info_paging"] = $this->_build_pagination($page, $total_info, $this->limit, base_url()."info/");

		$this->render_view('info/index', $data, false, 'public_sidebar');
		//$this->render_view('homepage/index', $data, false, 'public');
	}

	public function detil($int_pengaduan_id){

		$res = $this->model->get_info_detail($int_pengaduan_id);
		
		if(empty($res)){
			$this->render_view(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$info_respon = $this->model->get_info_respon($int_pengaduan_id);
			$data['data']		= $res;
			$data['respon']		= $info_respon;
			$this->render_view('info/detil', $data, false, 'public_sidebar');
		}
	}
}
