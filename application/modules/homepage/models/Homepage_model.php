<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Homepage_model extends MY_Model {

	public function create_pengaduan($ins){
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['dt_pengaduan'] = date("Y-m-d H:i:s");
		$ins['int_type'] = 1;
		$this->db->trans_begin();

		$ins = $this->clearFormInsert($ins, ['ci_csrf_token']);
		$this->db->insert($this->t_pengaduan, $ins);
		$int_pengaduan_id = $this->db->insert_id();
		$this->setQueryLog('ins_pengaduan');

		$no_tiket = $this->create_no_tiket_pengaduan($int_pengaduan_id);

		if ($this->db->trans_status() === FALSE || $no_tiket === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $no_tiket;
		}
	}

	function create_no_tiket_pengaduan($int_pengaduan_id){
		$substr_id = substr($int_pengaduan_id,-3);
		$str_pad_id = str_pad($substr_id,3,"0",STR_PAD_LEFT);
		$date_now = date("dmy");
		$no_tiket = 'ADU-'.$date_now.'-'.$str_pad_id;

		$upd['var_no_tiket'] = $no_tiket;

		$this->db->where('int_pengaduan_id', $int_pengaduan_id);
		$this->db->update($this->t_pengaduan, $upd);
		$this->setQueryLog('create_no_tiket');
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $no_tiket;
		}
	}

	public function create_informasi($ins){
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['dt_pengaduan'] = date("Y-m-d H:i:s");
		$ins['int_type'] = 2;
		$this->db->trans_begin();

		$ins = $this->clearFormInsert($ins, ['ci_csrf_token']);
		$this->db->insert($this->t_pengaduan, $ins);
		$int_pengaduan_id = $this->db->insert_id();
		$this->setQueryLog('ins_informasi');

		$no_tiket = $this->create_no_tiket_informasi($int_pengaduan_id);

		if ($this->db->trans_status() === FALSE || $no_tiket === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $no_tiket;
		}
	}

	function create_no_tiket_informasi($int_pengaduan_id){
		$substr_id = substr($int_pengaduan_id,-3);
		$str_pad_id = str_pad($substr_id,3,"0",STR_PAD_LEFT);
		$date_now = date("dmy");
		$no_tiket = 'INFO-'.$date_now.'-'.$str_pad_id;

		$upd['var_no_tiket'] = $no_tiket;

		$this->db->where('int_pengaduan_id', $int_pengaduan_id);
		$this->db->update($this->t_pengaduan, $upd);
		$this->setQueryLog('create_no_tiket');
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $no_tiket;
		}
	}
	
	function get_list_pengaduan(){
		$limit = 5;
		$offset = 0;
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4')
					->where('int_status = 1')
					->where('int_type = 1')
					->order_by('created_at', 'DESC')
					->limit($limit, $offset);
		
		return $this->db->get()->result();
	}

	function get_list_informasi(){
		$limit = 5;
		$offset = 0;
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4')
					->where('int_status = 1')
					->where('int_type = 2')
					->order_by('created_at', 'DESC')
					->limit($limit, $offset);
		
		return $this->db->get()->result();
	}

    public function search_list($keyword){
        return $this->callProcedure("rpt_data_pengaduan(?)",
                                    [$keyword], 'result');
    }
}