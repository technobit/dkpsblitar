<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Info_model extends MY_Model {

    public function get_info_list($start, $limit){
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4')
					->where('int_type = 2')
					->where('int_status = 1')
					->order_by('created_at', 'DESC')
					->limit($limit, $start);
		
		return $this->db->get()->result();
    }

    public function count_info_list(){
		$this->db->select("*")
					->from($this->t_pengaduan)
					->where('int_progress = 4')
					->where('int_type = 2')
					->where('int_status = 1 ');

		return $this->db->count_all_results();
    }

	public function get_info_detail($int_pengaduan_id){
		return $this->db->select("*")
					->get_where($this->t_pengaduan, ['int_pengaduan_id' => $int_pengaduan_id])->row();
	}

    public function get_info_respon($int_pengaduan_id){
		$this->db->select("*")
					->from($this->t_pengaduan_respon)
					->where('int_pengaduan_id', $int_pengaduan_id)
					->order_by('dt_respon', 'DESC');
		
		return $this->db->get()->result();
    }

	   public function get_info_nik($var_nik){
		return $this->db->select("*")
					->get_where($this->t_pengaduan, ['var_nik' => $var_nik])->row();
	}
}