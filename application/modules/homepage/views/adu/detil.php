<div class="col-lg-8">
	<div class="card">
	<h4 style="margin-bottom:10px"></span> <?=$data->var_topik?></h4>
		<div class="timeline">
			<!-- timeline time label -->
			<div class="time-label">
				<span class="bg-danger" style="color:#fff"><?=$data->var_kategori?></span>
				<?php if($data->int_progress == "0"){?>
					<span class="badge badge-pill bg-dark mb-1">Draft</span>
				<?php } else if ($data->int_progress == 1){?>
					<span class="badge badge-pill bg-warning mb-1">Menunggu Persetujuan</span>
				<?php } else if ($data->int_progress == 2){?>
					<span class="badge badge-pill bg-danger mb-1">Ditolak</span>
				<?php } else if ($data->int_progress == 3){?>
					<span class="badge badge-pill bg-primary mb-1">Diproses</span>
				<?php } else if ($data->int_progress == 4){?>
					<span class="badge badge-pill bg-success mb-1">Selesai</span>
				<?php } ?>
			</div>
			<div>
				<i class="fas fa-comments bg-danger" style="color:#fff"></i>
				<div class="timeline-item">
					<span class="time">
						<i class="fas fa-map-marker-alt"></i> <?=$data->var_alamat?> <i style="margin-right:5px"></i>
						<i class="fas fa-clock"></i> <?=idn_date($data->created_at, 'l, j F Y H:i:s')?>
					</span>
					<h3 class="timeline-header"><?=$data->var_nama?></h3>
					<div class="timeline-body">
						<?=$data->txt_note?>
					</div>
					<div class="timeline-footer">

					</div>
				</div>
			</div>
			<?php foreach($respon as $res){?>
			<div>
				<i class="fas fa-comment-dots bg-success" style="color:#fff"></i>
				<div class="timeline-item">
					<span class="time">
						<i class="fas fa-clock"></i> <?=idn_date($res->dt_respon, 'l, j F Y H:i:s')?>
					</span>
					<h3 class="timeline-header">Tanggapan</h3>
					<div class="timeline-body">
						<?=$res->txt_respon?>
					</div>
					<div class="timeline-footer">

					</div>
				</div>
			</div>
			<?php } ?>
			<div>
				<i class="fas fa-clock bg-gray"></i>
			</div>
		</div>
	</div>
</div>

