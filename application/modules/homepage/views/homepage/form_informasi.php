<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="form_informasi" width="80%">
<div id="modal-posts" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title"><?=$title ?></h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="row" id="data_informasi">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group row mb-1">
								<div class="col-lg-6">
									<label class="col-form-label">NIK/Nomor KTP</label>
									<input type="text" class="form-control form-control-sm" id="var_nik" placeholder="Masukkan NIK/Nomor KTP (16 Digit)" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>" />
								</div>
								<div class="col-lg-6">
									<label class="col-form-label">Nama Lengkap</label>
									<input type="text" class="form-control form-control-sm" id="var_nama" placeholder="Masukkan Nama Lengkap Anda" name="var_nama" value="<?=isset($data->var_nama)? $data->var_nama : ''?>" />
								</div>
								<div class="col-lg-12">
									<label class="col-form-label">Alamat</label>
									<select id="var_alamat" name="var_alamat" class="form-control form-control-sm select-2" style="width: 100%;">
									<option value="">- Pilih Kelurahan -</option>
									<?php 
										foreach($kelurahan as $kel){
											echo '<option value="'.$kel->var_kelurahan.'">'.$kel->var_kelurahan.'</option>';
										}
									?>
										<option value="LUAR KOTA BLITAR">LUAR KOTA BLITAR</option>
									</select>
								</div>
								<div class="col-lg-6">
									<label class="col-form-label">Nomor Ponsel</label>
									<input type="text" class="form-control form-control-sm" id="var_no_hp" placeholder="Masukkan Nomor Ponsel Anda" name="var_no_hp" value="<?=isset($data->var_no_hp)? $data->var_no_hp : ''?>" />
								</div>
								<div class="col-lg-6">
									<label class="col-form-label">Email</label>
									<input type="text" class="form-control form-control-sm" id="var_email" placeholder="Masukkan Alamat Email" name="var_email" value="<?=isset($data->var_email)? $data->var_email : ''?>" />
								</div>
								<div class="col-lg-4">
									<label class="col-form-label">Kategori Informasi</label>
									<select id="int_kategori_id" name="int_kategori_id" class="form-control form-control-sm select-2" style="width: 100%;">
									<option value="">- Pilih Kategori -</option>
									<?php 
										foreach($kategori as $kat){
											echo '<option value="'.$kat->int_kategori_id.'">'.$kat->var_kategori.'</option>';
										}
									?>
									</select>
								</div>
								<div class="col-lg-8">
									<label class="col-form-label">Judul/Topik Informasi</label>
									<input type="text" class="form-control form-control-sm" id="var_topik" placeholder="Masukkan Judul/Topik Informasi" name="var_topik" value="<?=isset($data->var_topik)? $data->var_topik : ''?>" />
								</div>
							</div>
							<div class="form-group row mb-1">
								<div class="col-lg-12">
									<label class="col-form-label">Detil Informasi</label>
									<textarea class="form-control form-control-sm textarea" name="txt_note" id="txt_note" placeholder="Enter text ..."><?=isset($data->txt_note)? $data->txt_note : ''?></textarea>
								</div>
							</div>
				
							<div class="recaptcha-error">
								
								<!-- BEGIN: ReCAPTCHA implementation example. -->
								<div id="recaptcha-demo" class="g-recaptcha" data-sitekey="6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" data-callback="onSuccess" data-action="action"></div>
							
								<script nonce="jKkOI9e7uuYHnpIlX8wdNQ">
								var onSuccess = function(response) {
									var errorDivs = document.getElementsByClassName("recaptcha-error");
									if (errorDivs.length) {
									errorDivs[0].className = "";
									}
									var errorMsgs = document.getElementsByClassName("recaptcha-error-message");
									if (errorMsgs.length) {
									errorMsgs[0].parentNode.removeChild(errorMsgs[0]);
									}
									};
								</script><!-- Optional noscript fallback. -->
								<noscript>
									<div style="width: 302px; height: 462px;">
										<iframe src="https://www.google.com/recaptcha/api/fallback?k=6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" frameborder="0" scrolling="no"></iframe>
										<div>
											<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response"></textarea>
										</div>
									</div><br>
								</noscript>
								<!-- END: ReCAPTCHA implementation example. -->
								<div class="recaptcha-error-message">Please verify that you are not a robot.</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="row" id="msg_informasi">
				
			</div>
		</div>
		<div class="modal-footer" id="footer_action">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('#txt_note').summernote({
			dialogsInBody: true,
			height: 200,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			toolbar: [
				// [groupName, [list of button]]
				['style', ['bold', 'italic', 'underline']],
				['para', ['ul', 'ol', 'paragraph']],
				['insert', ['picture', 'link', 'table']]
			],
			focus: true,                  // set focus to editable area after initializing summernote
			callbacks: {
				onImageUpload: function(image) {
					summernote_up_img(image[0]);
				},
				onMediaDelete : function(target) {
					summernote_del_img(target[0].src);
				}
			}
		});

		function summernote_up_img(image) {
            var data = new FormData();
			data.append("image", image);
			$.ajax({
				url: "<?php echo site_url('summernote/upload_image')?>",
				cache: false,
				contentType: false,
				processData: false,
				dataType:  'json',
				data: data,
				type: "POST",
				success: function(url) {
					$('#txt_note').summernote("insertImage", url.data);
				}
			});
		}

		function summernote_del_img(src) {
			$.ajax({
				data: {src : src},
				type: "POST",
				url: "<?php echo site_url('summernote/delete_image')?>",
				cache: false,
				dataType:  'json',
				success: function(url) {
					console.log(url);
				}
			});
		}

		$('.select-2').select2({dropdownParent: $('#ajax-modal')});
		$("#form_informasi").validate({
			rules: {
			    var_nik:{
			        required: true,
					digits : true,
					minlength: 16,
					maxlength: 16
				},
			    var_nama:{
			        required: true,
					minlength: 4
				},
			    var_alamat:{
			        required: true
				},
			    var_no_hp:{
			        required: true,
					digits : true,
					//minlength: 9,
					maxlength: 13
				},
			    int_kategori_id:{
			        required: true
				},
				var_topik:{
			        required: true,
					minlength: 10
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content"),
							"var_kategori" : $("#int_kategori_id option:selected").html()},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							document.getElementById("data_informasi").innerHTML  = ""; 
							document.getElementById("footer_action").innerHTML  = ""; 
							document.getElementById("msg_informasi").innerHTML  = '<div class="col-12">'+
										'<div class="callout callout-success">'+
											'<h2>Hai '+data.nama+', informasi kamu berhasil diajukan</h2>'+
											'<h4>Dengan nomor informasi <b>'+data.no_tiket+'</b></h4>'+
										'</div>'+
									'</div>'; 
							//resetForm('#form_informasi');
							//dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>