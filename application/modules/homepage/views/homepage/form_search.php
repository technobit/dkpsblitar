<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="form_search" width="80%">
<div id="modal-posts" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title"><?=$title ?></h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="row" id="data_informasi">
				<div class="col-lg-12">
					<div class="form-group row mb-1">
						<label class="col-form-label">Masukkan NIK atau Nomor Tiket Pelaporan</label>
						<input type="text" class="form-control form-control-sm" id="keyword" placeholder="Masukkan NIK atau Nomor Tiket Pelaporan" name="keyword" value="<?=isset($data->var_nik)? $data->var_nik : ''?>" />
					</div>
				</div>
			</div>
			<div class="row" id="msg_informasi">
				
			</div>
		</div>
		<div class="modal-footer" id="footer_action">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Cari</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#form_search").validate({
			rules: {
			    keyword:{
			        required: true
					minlength: 4
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							document.getElementById("data_informasi").innerHTML  = ""; 
							document.getElementById("footer_action").innerHTML  = ""; 
							document.getElementById("msg_informasi").innerHTML  = '<div class="col-12">'+
										'<div class="callout callout-success">'+
											'<h2>Hai '+data.nama+', informasi kamu berhasil diajukan</h2>'+
											'<h4>Dengan nomor informasi <b>'+data.no_tiket+'</b></h4>'+
										'</div>'+
									'</div>'; 
							//resetForm('#form_search');
							//dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>