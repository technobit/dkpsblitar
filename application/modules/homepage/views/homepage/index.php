<div class="row" id="input_pelaporan">
	<div class="col-lg-4 col-12">
		<div class="info-box bg-gradient-danger ajax_modal" data-block="body" data-url="<?=$url_add_pengaduan?>" style="cursor:pointer">
			<span class="info-box-icon" style="font-size:60px"><i class="fas fa-comments"></i></span>
			<div class="info-box-content">
				<span class="info-box-text"><b style="font-size:30px">PENGADUAN</b></span>
				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description">
					Sampaikan Pengaduan Anda
				</span>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-12">
		<div class="info-box bg-gradient-primary ajax_modal" data-block="body" data-url="<?=$url_add_informasi?>" style="cursor:pointer">
			<span class="info-box-icon" style="font-size:60px"><i class="fas fa-info-circle"></i></span>
			<div class="info-box-content">
			<span class="info-box-text"><b style="font-size:30px">INFORMASI</b></span>

			<div class="progress">
				<div class="progress-bar" style="width: 100%"></div>
			</div>
			<span class="progress-description">
				Sampaikan Informasi Anda
			</span>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-12">
		<div class="info-box bg-gradient-success ajax_modal" data-block="body" data-url="<?=$url_cek_pengaduan?>" style="cursor:pointer">
			<span class="info-box-icon" style="font-size:60px"><i class="fas fa-search"></i></span>
			<div class="info-box-content">
			<span class="info-box-text"><b style="font-size:30px">CEK PELAPORAN</b></span>
			<div class="progress">
				<div class="progress-bar" style="width: 100%"></div>
			</div>
			<span class="progress-description">
				Pantau Pelaporan Anda
			</span>
			</div>
		</div>
	</div>
</div>
<div class="row" id="list_pelaporan">
	<div class="col-lg-6 col-12">
		<div style="height:3px;border-top:solid 3px #dc3545"></div>
		<h3 class="mb-4">Pengaduan Masyarakat</h3>
		<?=$list_pengaduan?>
	</div>
	<div class="col-lg-6 col-12">
		<div style="height:3px;border-top:solid 3px #1988ff"></div>
		<h3 class="mb-4">Informasi Masyarakat</h3>
		<?=$list_informasi?>
	</div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
