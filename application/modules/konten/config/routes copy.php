<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['pengaduan']['get']                          = 'konten/pengaduan';
$route['pengaduan']['post']         			    = 'konten/pengaduan/list';
$route['pengaduan/add']['get']      			    = 'konten/pengaduan/add';
$route['pengaduan/save']['post']    			    = 'konten/pengaduan/save';
$route['pengaduan/([0-9]+)/get']['get']             = 'konten/pengaduan/get/$1';
$route['pengaduan/([0-9]+)']['get']                 = 'konten/pengaduan/get/$1';
$route['pengaduan/([0-9]+)']['post']                = 'konten/pengaduan/update/$1';
$route['pengaduan/([0-9]+)/del']['get']             = 'konten/pengaduan/confirm/$1';
$route['pengaduan/([0-9]+)/del']['post']            = 'konten/pengaduan/delete/$1';
$route['pengaduan/([0-9]+)/lampiran']['get']        = 'konten/pengaduan/confirm_del_lampiran/$1';
$route['pengaduan/([0-9]+)/lampiran_del']['post']   = 'konten/pengaduan/delete_lampiran/$1';

$route['informasi']['get']                          = 'konten/informasi';
$route['informasi']['post']         			    = 'konten/informasi/list';
$route['informasi/add']['get']      			    = 'konten/informasi/add';
$route['informasi/save']['post']    			    = 'konten/informasi/save';
$route['informasi/([0-9]+)/get']['get']             = 'konten/informasi/get/$1';
$route['informasi/([0-9]+)']['get']                 = 'konten/informasi/get/$1';
$route['informasi/([0-9]+)']['post']                = 'konten/informasi/update/$1';
$route['informasi/([0-9]+)/del']['get']             = 'konten/informasi/confirm/$1';
$route['informasi/([0-9]+)/del']['post']            = 'konten/informasi/delete/$1';
$route['informasi/([0-9]+)/lampiran']['get']        = 'konten/informasi/confirm_del_lampiran/$1';
$route['informasi/([0-9]+)/lampiran_del']['post']   = 'konten/informasi/delete_lampiran/$1';

$route['tanggapan']['get']                          = 'konten/tanggapan';                   //index
$route['tanggapan']['post']         			    = 'konten/tanggapan/list';              //list konten
$route['tanggapan/list/([0-9]+)']['get']            = 'konten/tanggapan/detil_pengaduan/$1';//detil konten dan tanggapan
//$route['tanggapan/list/([0-9]+)']['post']           = 'konten/tanggapan/list_tanggapan/$1'; //list tanggapan
$route['tanggapan/([0-9]+)/add']['get']             = 'konten/tanggapan/add/$1';            //tambah tanggapan - index_action
$route['tanggapan/save']['post']                    = 'konten/tanggapan/save';              //simpan tanggapan baru

$route['tanggapan/([0-9]+)']['get']                 = 'konten/tanggapan/get_tanggapan/$1';  //detil tanggapan
$route['tanggapan/([0-9]+)']['post']                = 'konten/tanggapan/upd_tanggapan/$1';  //update tanggapan
$route['tanggapan/([0-9]+)/del']['get']             = 'konten/tanggapan/confirm/$1';        //confirm del tanggapan
$route['tanggapan/([0-9]+)/del']['post']            = 'konten/tanggapan/delete/$1';         //delete tanggapan

$route['detil/tanggapan/([0-9]+)/img']['get']       = 'konten/detil/get_img/$1';             //tambah tanggapan - index_action
//$route['detil/([0-9]+)/det']['get']           = 'konten/detil/detail/$1';          //detil tanggapan - index_action

$route['detil/tanggapan/([0-9]+)/upd']['post']  = 'konten/detil/update/$1';
$route['detil/tanggapan/([0-9]+)/lampiran']['get']      = 'konten/detil/confirm_del_lampiran/$1';
$route['detil/tanggapan/([0-9]+)/lampiran_del']['post'] = 'konten/detil/delete_lampiran/$1';
