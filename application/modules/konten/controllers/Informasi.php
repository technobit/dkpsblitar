<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Informasi extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'INFORMASI'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'konten';
		$this->routeURL = 'informasi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('informasi_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
		//$this->load->model('master/sumber_model', 'sumber');
		//$this->load->model('master/kategori_model', 'kategori');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Informasi';
		$this->page->menu 	  = 'informasi';
		$this->page->submenu1 = '';
		$this->breadcrumb->title = 'Informasi Masyarakat';
		$this->breadcrumb->card_title = 'Daftar informasi Masyarakat';
		$this->breadcrumb->icon = 'fas fa-info-circle';
		$this->breadcrumb->list = ['Daftar informasi Masyarakat'];
		$this->css = true;
		$this->js = true;
		$data['kategori_list'] = $this->kategori->get_list();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('informasi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true),$filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){

			$action = '<div style="width:120px;display:inline-block">';
			$i++;
			//hapus
			$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_informasi_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" style="width:30px;margin-right:5px"><i class="fa fa-trash"></i></a>';
			//verifikasi
			$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_informasi_id.'/get" class="ajax_modal btn btn-xs btn-success tooltips" data-placement="top" data-original-title="Verifikasi" style="width:85px"><i class="fa fa-check"></i> Verifikasi</a> ';
			$action .= '</div>';

			$data[] = array($i, $d->var_no_tiket, idn_date($d->dt_informasi, 'j F Y H:i:s'), $d->var_nama, $d->var_nik, $d->var_kategori, $d->var_topik, $d->int_progress, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								'filter_start' => $filter_start,
								'filter_end' => $filter_end,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function get($int_informasi_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_informasi_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/$int_informasi_id");
			$data['title']		= 'Ubah Data informasi';
			$this->load_view('informasi/index_action', $data);
		}
		
	}

	public function update($int_informasi_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('int_progress', 'Status Verifikasi', 'required');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$update = $this->model->update($int_informasi_id, $this->input->post());
			
			$this->set_json([ 'stat' =>  ($update)? true : false, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}

	public function confirm($int_informasi_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_informasi_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_informasi_id/del");
			$data['title']	= 'Hapus informasi';
			$data['info']   = ['No. Tiket' => $res->var_no_tiket,
								'Kategori' => $res->var_kategori,
								'Topik' => $res->var_topik,
								'Detil' => $res->txt_note];
			$this->load_view('informasi/index_delete', $data);
		}
	}

	public function delete($int_informasi_id){
		$this->authCheckDetailAccess('d');
		$delete = $this->model->delete($int_informasi_id);
		$this->set_json([  'stat' => ($delete)? true : false,  
							'mc' => $delete, //modal close
							'msg' => ($delete)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

}
