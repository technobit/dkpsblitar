<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Pengaduan extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'PENGADUAN'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'konten';
		$this->routeURL = 'pengaduan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('pengaduan_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
		//$this->load->model('master/sumber_model', 'sumber');
		//$this->load->model('master/kategori_model', 'kategori');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pengaduan';
		$this->page->menu 	  = 'pengaduan';
		$this->page->submenu1 = '';
		$this->breadcrumb->title = 'Pengaduan Masyarakat';
		$this->breadcrumb->card_title = 'Daftar Pengaduan Masyarakat';
		$this->breadcrumb->icon = 'fas fa-comments';
		$this->breadcrumb->list = ['Daftar Pengaduan Masyarakat'];
		$this->css = true;
		$this->js = true;
		$data['kategori_list'] = $this->kategori->get_list('int_pengaduan');
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('pengaduan/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true),$filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$action = '<div style="width:120px;display:inline-block">';
			$i++;
			//Type
			if($d->int_type == 1){
				$type = '<span class="badge bg-danger">Pengaduan</span>';
			}else if($d->int_type == 2){
				$type = '<span class="badge bg-primary">Informasi</span>';
			}else{
				$type = '';
			}
			
			if(isset($d->int_pengaduan_respon_id)){ //Update Tanggapan //(3) Ditanggapi, (4) Selesai  : edit tanggapan & status selesai 
				//tanggapan & status
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_respon_id.'/tanggapan" class="ajax_modal btn btn-xs btn-success tooltips" data-placement="top" data-original-title="Tanggapan" style="width:120px"><i class="fas fa-comment-dots"></i> Tanggapan</a> ';	
			}else{ //(0) Draft, (1) Proses disposisi, (2) Ditolak : hapus dan disposisi
				//hapus
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" style="width:30px;margin-right:5px"><i class="fa fa-trash"></i></a>';
				//disposisi
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_id.'/get" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Verifikasi" style="width:85px"><i class="fa fa-share"></i> Disposisi</a> ';
			}

			$action .= '</div>';

			$data[] = array($i, $d->var_no_tiket, idn_date($d->dt_pengaduan, 'j F Y H:i:s'), $d->var_nama, $d->var_nik, $type, $d->var_kategori, $d->var_topik, $d->int_progress, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								'filter_start' => $filter_start,
								'filter_end' => $filter_end,
								$this->getCsrfName() => $this->getCsrfToken()));
	}
	
	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Data Pengaduan';
		$data['kategori']	= $this->kategori->get_list();
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('pengaduan/index_action', $data, true);
	}
	
	public function get($int_pengaduan_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pengaduan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/$int_pengaduan_id");
			$data['disposisi']	= $this->model->get_disposisi();
			$data['title']		= 'Ubah Data Pengaduan';
			$this->load_view('pengaduan/index_action', $data);
		}
	}

	public function update($int_pengaduan_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('int_progress', 'Status Verifikasi', 'required');
		/*if($this->input->post('int_progress') == 1){
			$this->form_validation->set_rules('int_user_id', 'Tujuan Disposisi Pengaduan', 'required');			
		}*/
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$update = $this->model->update($int_pengaduan_id, $this->input->post());
			
			$this->set_json([ 'stat' =>  ($update)? true : false, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}

	public function get_tanggapan($int_pengaduan_respon_id){ //get tanggapan
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_tanggapan($int_pengaduan_respon_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/{$int_pengaduan_respon_id}/tanggapan");
			$data['title']		= 'Ubah Data tanggapan';
			$this->load_view('pengaduan/index_action_tanggapan', $data);
		}
		
	}

	public function update_tanggapan($int_pengaduan_respon_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('txt_respon', 'Detil Tanggapan', 'required');			
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$update = $this->model->update_tanggapan($int_pengaduan_respon_id, $this->input->post());
			
			$this->set_json([ 'stat' =>  ($update)? true : false, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}
	
	public function confirm($int_pengaduan_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_pengaduan_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_pengaduan_id/del");
			$data['title']	= 'Hapus Pengaduan';
			$data['info']   = ['No. Tiket' => $res->var_no_tiket,
								'Kategori' => $res->var_kategori,
								'Topik' => $res->var_topik,
								'Detil' => $res->txt_note];
			$this->load_view('pengaduan/index_delete', $data);
		}
	}

	public function delete($int_pengaduan_id){
		$this->authCheckDetailAccess('d');
		$delete = $this->model->delete($int_pengaduan_id);
		$this->set_json([  'stat' => ($delete)? true : false,  
							'mc' => $delete, //modal close
							'msg' => ($delete)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

}
