<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class tanggapan extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'tanggapan'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'konten';
		$this->routeURL = 'tanggapan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('tanggapan_model', 'model');
		$this->load->model('pengaduan_model', 'pengaduan');
		$this->load->model('master/kategori_model', 'kategori');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Tanggapan Terhadap Pengaduan';
		$this->page->menu 	  = 'tanggapan';
		$this->page->submenu1 = '';
		$this->breadcrumb->title = 'Tanggapan Terhadap Pengaduan';
		$this->breadcrumb->card_title = 'Daftar Pengaduan Masyarakat';
		$this->breadcrumb->icon = 'fas fa-comment-dots';
		$this->breadcrumb->list = ['Tanggapan Terhadap Pengaduan'];
		$this->css = true;
		$this->js = true;
		$data['kategori_list'] = $this->kategori->get_list();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('tanggapan/index', $data, true);
	}

	public function list(){ //list pengaduan
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true),$filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kategori_filter', true), $this->input->post('progress_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);

		foreach($ldata as $d){
			$action = '<div>';
			$i++;
			//Type
			if($d->int_type == 1){
				$type = '<span class="badge bg-danger">Pengaduan</span>';
			}else if($d->int_type == 2){
				$type = '<span class="badge bg-primary">Informasi</span>';
			}else{
				$type = '';
			}

			if(isset($d->int_pengaduan_respon_id)){ //Update
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_respon_id.'/get" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Edit data" style="width:40px;"><i class="fa fa-comment-dots"></i></a>';		
			}else{ //insert
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->pengaduan_id.'/add" class="ajax_modal btn btn-sm btn-danger tooltips" data-placement="top" data-original-title="Add data" style="width:40px;"><i class="fa fa-comment-medical"></i></a>';		
			}
			$action .= '</div>';

			$data[] = array($i, $d->var_no_tiket, idn_date($d->dt_pengaduan, 'j F Y H:i:s'), $d->var_nama, $type, $d->var_kategori, $d->var_topik, $d->int_progress, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								'filter_start' => $filter_start,
								'filter_end' => $filter_end,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add($int_pengaduan_id){ //detail pengaduan
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup
		$res = $this->model->get_pengaduan($int_pengaduan_id);

		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/{$int_pengaduan_id}/ins");
			$data['title']		= 'Form Tanggapan';
			$this->load_view('tanggapan/index_action', $data, true);
		}
	}
	
	public function insert($int_pengaduan_id){ //save create tanggapan
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('txt_respon', 'Detil Tanggapan', 'required');			
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$post = $this->input->post();
			$post['int_pengaduan_id'] = $int_pengaduan_id;
			$insert = $this->model->create($post);
			
			$this->set_json([ 'stat' =>  ($insert)? true : false, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}

	public function get($int_pengaduan_respon_id){ //get tanggapan
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_tanggapan($int_pengaduan_respon_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/{$int_pengaduan_respon_id}/upd");
			$data['title']		= 'Ubah Data tanggapan';
			$this->load_view('tanggapan/index_action', $data);
		}
		
	}

	public function update($int_pengaduan_respon_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('txt_respon', 'Detil Tanggapan', 'required');			
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }else{
			$update = $this->model->update($int_pengaduan_respon_id, $this->input->post());
			
			$this->set_json([ 'stat' =>  ($update)? true : false, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);
		}
	}
}
