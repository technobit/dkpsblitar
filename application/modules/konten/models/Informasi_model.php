<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Informasi_model extends MY_Model {

    public function list($kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->t_informasi)
					->where('`int_status` = 1 ');

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_informasi', $filter_start, $filter_end);
		}

		if(($kategori_filter!="")){ // filter
            $this->db->where('int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('`int_progress`', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_no_tiket', $filter)
					->or_like('var_topik', $filter)
					->or_like('txt_note', $filter)
					->group_end();
		}

		$order = 'dt_informasi';
		switch($order_by){
			case 1 : $order = 'dt_informasi '; break;
			case 2 : $order = 'var_topik '; break;
			default: {$order = 'dt_informasi '; $sort = 'DESC';} break;
		}
		
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL){
		$this->db->select("*")
					->from($this->t_informasi)
					->where('`int_status` = 1 ');

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_informasi', $filter_start, $filter_end);
		}

		if(($kategori_filter!="")){ // filter
            $this->db->where('int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('`int_progress`', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_no_tiket', $filter)
					->or_like('var_topik', $filter)
					->or_like('txt_note', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function get($int_informasi_id){
		return $this->db->select("*")
					->get_where($this->t_informasi, ['int_informasi_id' => $int_informasi_id])->row();

	}

	public function update($int_informasi_id, $upd){
		$upd['updated_at'] = date('Y-m-d H:i:s');
		$upd['updated_by'] = $this->session->userdata['user_id'];
		
		$this->db->trans_begin();

		$this->db->where('int_informasi_id', $int_informasi_id);
		$this->db->update($this->t_informasi, $upd);
		$this->setQueryLog('upd_informasi');

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	
	public function delete($int_informasi_id){
		$del['int_status'] = 0;
		$del['deleted_at'] = date('Y-m-d H:i:s');
		$del['deleted_by'] = $this->session->userdata['user_id'];
		
		$this->db->trans_begin();

		$this->db->where('int_informasi_id', $int_informasi_id);
		$this->db->update($this->t_informasi, $del);
		$this->setQueryLog('del_informasi');

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
