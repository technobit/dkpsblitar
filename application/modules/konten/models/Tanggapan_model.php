<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Tanggapan_model extends MY_Model {
	
    public function list($kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$this->db->select("*, tp.int_pengaduan_id as pengaduan_id")
					->from($this->t_pengaduan." tp")
					->join($this->t_pengaduan_respon." tt", "tp.int_pengaduan_id = tt.int_pengaduan_id", "left")
					->where('tp.int_status = 1 ')
					->where('tp.int_progress IN (1,3) ');

		if($this->session->userdata['group_id'] != 1){
			$this->db->where('tp.int_user_id', $this->session->userdata['user_id']);
		}
			
		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('tp.dt_pengaduan', $filter_start, $filter_end);
		}

		if(($kategori_filter!="")){ // filter
            $this->db->where('tp.int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('tp.int_progress', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tp.var_no_tiket', $filter)
					->or_like('tp.var_topik', $filter)
					->or_like('tp.txt_note', $filter)
					->group_end();
		}

		$order = 'dt_pengaduan';
		switch($order_by){
			case 1 : $order = 'tp.dt_pengaduan '; break;
			case 2 : $order = 'tp.var_topik '; break;
			default: {$order = 'tp.dt_pengaduan '; $sort = 'DESC';} break;
		}
		
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL){
		$this->db->select("*")
					->from($this->t_pengaduan." tp")
					->join($this->t_pengaduan_respon." tt", "tp.int_pengaduan_id = tt.int_pengaduan_id", "left")
					->where('tp.int_status = 1 ')
					->where('tp.int_progress IN (1,3) ');

		if($this->session->userdata['group_id'] != 1){
			$this->db->where('tp.int_user_id', $this->session->userdata['user_id']);
		}

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('tp.dt_pengaduan', $filter_start, $filter_end);
		}

		if(($kategori_filter!="")){ // filter
            $this->db->where('tp.int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('tp.int_progress', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tp.var_no_tiket', $filter)
					->or_like('tp.var_topik', $filter)
					->or_like('tp.txt_note', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function get_pengaduan($int_pengaduan_id){
		$data = $this->db->query("SELECT *
									FROM  {$this->t_pengaduan} tp
									LEFT JOIN {$this->t_pengaduan_respon} tt ON tp.int_pengaduan_id = tt.int_pengaduan_id
									WHERE tp.int_pengaduan_id = {$int_pengaduan_id}")->row_array();
		return (object) $data;
	}
	
	public function create($ins){
		$ins['dt_respon'] = date('Y-m-d H:i:s');
		$ins['created_at'] = date('Y-m-d H:i:s');
		$ins['created_by'] = $this->session->userdata['user_id'];
		
		$this->db->trans_begin();

		$this->db->insert($this->t_pengaduan_respon, $ins);
		$this->setQueryLog('ins_tanggapan');

		$upd_progress = $this->upd_progress($ins['int_pengaduan_id']);

		if ($this->db->trans_status() === FALSE || $upd_progress === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function get_tanggapan($int_pengaduan_respon_id){
		$data = $this->db->query("SELECT tp.*, ua.`var_nama_depan`, ua.`var_nama_belakang`, mb.`var_bidang` 
									FROM  {$this->t_pengaduan} tp
									LEFT JOIN {$this->t_pengaduan_respon} tt ON tp.int_pengaduan_id = tt.int_pengaduan_id
									LEFT JOIN m_user_admin ua ON tp.`int_user_id` = ua.`int_user_id`
									LEFT JOIN m_bidang mb ON ua.`int_bidang_id` = mb.`int_bidang_id`
									WHERE tt.int_pengaduan_respon_id = {$int_pengaduan_respon_id}")->row_array();
		return (object) $data;
	}
		
	public function update($int_pengaduan_respon_id, $upd){
		$upd['updated_at'] = date('Y-m-d H:i:s');
		$upd['updated_by'] = $this->session->userdata['user_id'];
		
		$this->db->trans_begin();

		$this->db->where('int_pengaduan_respon_id', $int_pengaduan_respon_id);
		$this->db->update($this->t_pengaduan_respon, $upd);
		$this->setQueryLog('upd_pengaduan_respon');

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function upd_progress($int_pengaduan_id){
		$upd['int_pengaduan_id'] = $int_pengaduan_id;
		$upd['int_progress'] = 3;
		
		$this->db->trans_begin();

		$this->db->where('int_pengaduan_id', $int_pengaduan_id);
		$this->db->update($this->t_pengaduan, $upd);
		$this->setQueryLog('upd_pengaduan_respon');

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
