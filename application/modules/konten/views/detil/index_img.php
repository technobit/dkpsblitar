<div id="modal-posts" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="row mb-1 img-popup">
				<?php if(isset($data)):
					foreach($data as $lmp):
						if($lmp->int_source==1){
							$img_url = $lmp->txt_dir;
						}else{
							$img_url = cdn_url().$lmp->txt_dir;
						}?>
					<div class="col-md-3 text-center" data-responsive="<?=$img_url?> 375, <?=$img_url?> 480, <?=$img_url?> 800" data-src="<?=$img_url?>" data-sub-html="<?=$lmp->txt_desc?>">
						<a href="<?=$img_url?>">
							<img class="img-thumb-sm" src="<?=$img_url?>">
						</a>
							<p><?=$lmp->txt_desc?></p>
					</div>
				<?php endforeach; endif;?>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$(".img-popup").lightGallery();
	});
</script>