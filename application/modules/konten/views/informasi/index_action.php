<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="posts-form" width="80%">
<div id="modal-posts" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Detil Informasi Masyarakat</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="row">
				<div class="col-md-7">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Identitas Pelapor</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<dd class="col-sm-3">NIK</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8"><?=isset($data->var_nik)? $data->var_nik : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Nama</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_nama)? $data->var_nama : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Alamat</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_alamat)? $data->var_alamat : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">No. Ponsel</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_no_hp)? $data->var_no_hp : ''?></dt>
							</div>
						</div>
					</div>
					<div class="card card-danger">
						<div class="card-header">
							<h3 class="card-title">Data informasi</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Tanggal</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8"><?=isset($data->dt_informasi)? idn_date($data->dt_informasi, 'j F Y H:i:s') : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Kategori</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_kategori)? $data->var_kategori : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Topik</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_topik)? $data->var_topik : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Detil</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dd class="col-sm-8"><?=isset($data->txt_note)? $data->txt_note : ''?></dd>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="card card-success">
						<div class="card-header">
							<h3 class="card-title">Verifikasi informasi</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<h5>Dengan ini saya menyatakan bahwa informasi ini telah diverifikasi serta akan dipublikasikan kepada masyarakat.</h5>
								<div class="col-sm-12 icheck-success">
									<input type="radio" id="int_progress1" name="int_progress" value="1" <?=isset($data->int_progress)? (($data->int_progress == 1)? 'checked' : '') : '' ?> onchange="check_verif()">
										<label for="int_progress1">Ya, informasi terverifikasi</label>
								</div>
								<div class="col-sm-12 icheck-danger">
									<input type="radio" id="int_progress2" name="int_progress" value="2" <?=isset($data->int_progress)? (($data->int_progress == 2)? 'checked' : '') : '' ?> onchange="check_verif()">
									<label for="int_progress2">Tidak, informasi ini ditolak</label>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>

	$(document).ready(function(){

		$('.select-2').select2({dropdownParent: $('#ajax-modal')});
		
		$("#posts-form").validate({
			rules: {
			    int_progress:{
			        required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#posts-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>