<style>
.note-editor.note-frame.card{
	margin-bottom : 0px;
}
p >img {
	width:100%;
}
</style>
<?php
if($data->int_progress == 0){
	$progress = 'Draft';
	$progress_color = 'dark';
} else if ($data->int_progress == 1){
	$progress = 'Menunggu Persetujuan';
	$progress_color = 'warning';
} else if ($data->int_progress == 2){
	$progress = 'Ditolak';
	$progress_color = 'danger';
} else if ($data->int_progress == 3){
	$progress = 'Diproses';
	$progress_color = 'primary';
} else if ($data->int_progress == 4){
	$progress = 'Selesai';
	$progress_color = 'success';
}
?>
<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="posts-form" width="80%">
<div id="modal-posts" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<ul class="nav nav-tabs no-border-b" id="custom-content-below-tab" role="tablist" style="width:100%">
				<li class="nav-item">
					<a class="nav-link active" id="index-form-input-tab" data-toggle="pill" href="#index-form-input" role="tab" aria-controls="index-form-input" aria-selected="true"><b>PENGADUAN DAN TANGGAPAN</b></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="index-form-validasi-tab" data-toggle="pill" href="#index-form-validasi" role="tab" aria-controls="index-form-validasi" aria-selected="false"><b>VALIDASI</b></a>
				</li>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="tab-pane fade show active" id="index-form-input" role="tabpanel" aria-labelledby="index-form-input-tab">
					<div class="row">
						<div class="col-md-6">
							<div class="card card-danger">
								<div class="card-header">
									<h3 class="card-title">Data Pengaduan</h3>
								</div>
								<div class="card-body">
									<div class="form-group row mb-1">
										<dd class="col-sm-3">Tanggal</dd>
										<dt class="col-sm-1 text-right">:</dt>
										<dt class="col-sm-8"><?=isset($data->dt_pengaduan)? idn_date($data->dt_pengaduan, 'j F Y H:i:s') : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-3">Kategori</dd>
										<dt class="col-sm-1 text-right">:</dt>
										<dt class="col-sm-8" ><?=isset($data->var_kategori)? $data->var_kategori : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-3">Topik</dd>
										<dt class="col-sm-1 text-right">:</dt>
										<dt class="col-sm-8" ><?=isset($data->var_topik)? $data->var_topik : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-3">Detil</dd>
										<dt class="col-sm-1 text-right">:</dt>
										<dd class="col-sm-8"><?=isset($data->txt_note)? $data->txt_note : ''?></dd>
									</div>
								</div>
							</div>
							<div class="alert alert-<?=$progress_color?>">
								<h5><i class="icon fas fa-check"></i><?=$progress?></h5>
								Aspirasi telah ditanggapi oleh : <?=$data->var_nama_depan?> <?=$data->var_nama_belakang?></br>
								Bidang : <?=$data->var_bidang?></br>
								<?=isset($data->created_at)? idn_date($data->created_at, 'j F Y H:i:s') : ''?> 
							</div>
						</div>
						<div class="col-md-6">
							<div class="card card-warning" style="margin-bottom:0px !important">
								<div class="card-header">
									<h3 class="card-title">Data Tanggapan</h3>
								</div>
								<div class="card-body" style="padding:0px">
									<textarea class="form-control form-control-sm textarea" name="txt_respon" id="txt_respon" placeholder="Enter text ..."><?=isset($data->txt_respon)? $data->txt_respon : ''?></textarea>
								</div>
							</div>
						</div>
						<input type="hidden" id="int_pengaduan_id" name="int_pengaduan_id" value="<?=isset($data->int_pengaduan_id)? $data->int_pengaduan_id : ''?>"/>
					</div>
				</div>
				<div class="tab-pane fade" id="index-form-validasi" role="tabpanel" aria-labelledby="index-form-validasi-tab">
					<div class="row">
						<div class="col-md-12">
							<div class="callout callout-danger">
								<h5>Pernyataan Validasi Pengaduan dan Tanggapan</h5>

								<p>Dengan ini saya menyatakan bahwa pengaduan ini telah ditanggapi dan dinyatakan selesai serta akan dipublikasikan kepada masyarakat</p>

								<div class="form-group row mb-1">
									<div class="col-sm-12 mt-1">
										<div class="icheck-danger d-inline mr-5">
											<input type="radio" id="radio1" name="int_progress" value="3" <?=isset($data->int_progress)? (($data->int_progress == 3)? 'checked' : '') : 'checked' ?>>
												<label for="radio1">Tidak, proses pengaduan belum selesai </label>
										</div>
										<div class="icheck-success d-inline">
											<input type="radio" id="radio2" name="int_progress" value="4" <?=isset($data->int_progress)? (($data->int_progress == 4)? 'checked' : '') : '' ?>>
											<label for="radio2">Ya, pengaduan dinyatakan selesai</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('#txt_respon').summernote({
			dialogsInBody: true,
			height: 450,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			toolbar: [
				// [groupName, [list of button]]
				['style', ['bold', 'italic', 'underline']],
				['para', ['ul', 'ol', 'paragraph']],
				['insert', ['picture', 'link', 'table']]
			],
			focus: true,                  // set focus to editable area after initializing summernote
			callbacks: {
                    onImageUpload: function(image) {
                        summernote_up_img(image[0]);
                    },
                    onMediaDelete : function(target) {
                        summernote_del_img(target[0].src);
                    }
                }
		});

		function summernote_up_img(image) {
            var data = new FormData();
			data.append("image", image);
			$.ajax({
				url: "<?php echo site_url('summernote/upload_image')?>",
				cache: false,
				contentType: false,
				processData: false,
				dataType:  'json',
				data: data,
				type: "POST",
				success: function(url) {
					$('#txt_respon').summernote("insertImage", url.data);
				}
			});
		}

		function summernote_del_img(src) {
			$.ajax({
				data: {src : src},
				type: "POST",
				url: "<?php echo site_url('summernote/delete_image')?>",
				cache: false,
				dataType:  'json',
				success: function(url) {
					console.log(url);
				}
			});
		}
		
		$("#posts-form").validate({
			rules: {
			    int_progress:{
			        required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#posts-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>