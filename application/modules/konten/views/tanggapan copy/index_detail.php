<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-danger">
				<div class="card-header" data-card-widget="collapse" style="cursor:pointer">
					<h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="form-group row mb-1">
						<dt class="col-sm-2">Tanggal Pengaduan</dt>
						<dt class="col-sm-1 text-right">:</dt>
						<dd class="col-sm-9"><?=isset($data->dt_pengaduan)? idn_date($data->dt_pengaduan, 'j F Y H:i:s') : ''?></dd>
					</div>
					<div class="form-group row mb-1">
						<dt class="col-sm-2">Kategori Pengaduan</dt>
						<dt class="col-sm-1 text-right">:</dt>
						<dd class="col-sm-9" ><?=isset($data->var_kategori)? $data->var_kategori : ''?></dd>
					</div>
					<div class="form-group row mb-1">
						<dt class="col-sm-2">Topik Pengaduan</dt>
						<dt class="col-sm-1 text-right">:</dt>
						<dd class="col-sm-9" ><?=isset($data->var_topik)? $data->var_topik : ''?></dd>
					</div>
					<div class="form-group row mb-1">
						<dt class="col-sm-2">Detil Pengaduan</dt>
						<dt class="col-sm-1 text-right">:</dt>
						<dd class="col-sm-9"><?=isset($data->txt_note)? $data->txt_note : ''?></dd>
					</div>
				</div>
            </div>
        </section>
        <section class="col-lg-12">
            <div class="card">
				<div class="card-header" data-card-widget="collapse" style="cursor:pointer">
					<h3 class="card-title mt-1">
                        <i class="fas fa-comment-dots"></i>
                        Data Tanggapan
                    </h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-hover table-full-width" id="data_table">
					</table>
					<button type="button" data-block="body" class="btn btn-sm btn-danger ajax_modal" data-url="<?=$url?>" style="float: right;"><i class="fas fa-plus"></i> Tambah Tanggapan</button>
				</div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
