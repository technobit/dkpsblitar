<style>
.note-editor.note-frame.card{
	margin-bottom : 0px;
}
p >img {
	width:100%;
}
</style>
<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="posts-form" width="80%">
<div id="modal-posts" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title">Tanggapan Terhadap Pengaduan Masyarakat</h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="row">
				<div class="col-md-6">
					<div class="card card-danger">
						<div class="card-header">
							<h3 class="card-title">Data Pengaduan</h3>
						</div>
						<div class="card-body">
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Tanggal</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8"><?=isset($data->dt_pengaduan)? idn_date($data->dt_pengaduan, 'j F Y H:i:s') : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Kategori</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_kategori)? $data->var_kategori : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Topik</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dt class="col-sm-8" ><?=isset($data->var_topik)? $data->var_topik : ''?></dt>
							</div>
							<div class="form-group row mb-1">
								<dd class="col-sm-3">Detil</dd>
								<dt class="col-sm-1 text-right">:</dt>
								<dd class="col-sm-8"><?=isset($data->txt_note)? $data->txt_note : ''?></dd>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card card-warning" style="margin-bottom:0px !important">
						<div class="card-header">
							<h3 class="card-title">Data Tanggapan</h3>
						</div>
						<div class="card-body" style="padding:0px">
							<textarea class="form-control form-control-sm textarea" name="txt_respon" id="txt_respon" placeholder="Enter text ..."><?=isset($data->txt_respon)? $data->txt_respon : ''?></textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('#txt_respon').summernote({
			dialogsInBody: true,
			height: 450,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			toolbar: [
				// [groupName, [list of button]]
				['style', ['bold', 'italic', 'underline']],
				['para', ['ul', 'ol', 'paragraph']],
				['insert', ['picture', 'link', 'table']]
			],
			focus: true,                  // set focus to editable area after initializing summernote
			callbacks: {
				onImageUpload: function(image) {
					summernote_up_img(image[0]);
				},
				onMediaDelete : function(target) {
					summernote_del_img(target[0].src);
				}
			}
		});

		function summernote_up_img(image) {
            var data = new FormData();
			data.append("image", image);
			$.ajax({
				url: "<?php echo site_url('summernote/upload_image')?>",
				cache: false,
				contentType: false,
				processData: false,
				dataType:  'json',
				data: data,
				type: "POST",
				success: function(url) {
					$('#txt_respon').summernote("insertImage", url.data);
				}
			});
		}

		function summernote_del_img(src) {
			$.ajax({
				data: {src : src},
				type: "POST",
				url: "<?php echo site_url('summernote/delete_image')?>",
				cache: false,
				dataType:  'json',
				success: function(url) {
					console.log(url);
				}
			});
		}
		
		$("#posts-form").validate({
			rules: {
			    int_progress:{
			        required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#posts-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>