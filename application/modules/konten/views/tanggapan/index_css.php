<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.css">
<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/lightGallery/dist/css/lightgallery.css">
<style>
.img-thumb-sm {
    width: 100%;
    height: 100px;
    border: 0 solid rgba(0,0,0,.125);
    border-radius: 3px;
    padding: 5px;
    box-shadow: 0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);
}
.vertical-top{
    vertical-align:top !important;
}
</style>