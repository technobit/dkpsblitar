<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $('.kategori_filter').select2();
    $('.progress_filter').select2();


    var dataTable;
    $(document).ready(function() {
        dataTable = $('#data_table').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            //"order": [[ 1, "desc" ]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.kategori_filter = $('.kategori_filter').val();
					d.progress_filter = $('.progress_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "15", //nomor
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "100", //no tiket
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "150", //tanggal
                    "sClass": "text-right vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //nama
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //nik
                    "sClass": "text-right vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //kategori
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto",//topik
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "50", 
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "80",
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [7],//post type
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 1:
                                return '<span class="badge bg-danger">Belum Ditanggapi</span>';
                                break;
                            case 3:
                                return '<span class="badge bg-success">Sudah Ditanggapi</span>';
                                break;
                            case 4:
                                return '<span class="badge bg-primary">Selesai</span>';
                                break;
                            default:
                                return '<span class="badge bg-danger">Belum Ditanggapi</span>';
                                break;
                        }
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
		});

        $('.kategori_filter').change(function(){
            dataTable.draw();
        });
		
        $('.progress_filter').change(function(){
            dataTable.draw();
        });

    });
</script>