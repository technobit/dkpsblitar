<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['laporan_aspirasi']['get']           = 'laporan/laporan_aspirasi';
$route['laporan_aspirasi']['post']          = 'laporan/laporan_aspirasi/list';
$route['export/laporan_aspirasi']['post']   = 'laporan/laporan_aspirasi/export';

$route['rekap_aspirasi']['get']         = 'laporan/rekap_aspirasi';
$route['rekap_aspirasi']['post']        = 'laporan/rekap_aspirasi/list';
$route['export/rekap_aspirasi']['post'] = 'laporan/rekap_aspirasi/export';