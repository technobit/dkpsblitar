<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
	
class Laporan_aspirasi extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-ASPIRASI'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'laporan_aspirasi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('laporan_aspirasi_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Laporan Data Aspirasi';
		$this->page->menu = 'laporan';
		$this->page->submenu1 = 'laporan_aspirasi';
		$this->breadcrumb->title = 'Data Aspirasi Masyarakat';
		$this->breadcrumb->card_title = 'Data Aspirasi Masyarakat';
		$this->breadcrumb->icon = 'fas fa-file-alt';
		$this->breadcrumb->list = ['Laporan', 'Data Aspirasi Masyarakat'];
		$this->js = true;
		$this->css = true;
		$data['kategori_list'] = $this->kategori->get_list('int_pengaduan');
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('laporan_aspirasi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('tipe_filter', true), $this->input->post('kategori_filter', true), $this->input->post('progress_filter', true),$filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('tipe_filter', true), $this->input->post('kategori_filter', true), $this->input->post('progress_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$action = '<div style="width:120px;display:inline-block">';
			$i++;
			//Type
			if($d->int_type == 1){
				$type = '<span class="badge bg-danger">Pengaduan</span>';
			}else if($d->int_type == 2){
				$type = '<span class="badge bg-primary">Informasi</span>';
			}else{
				$type = '';
			}
			
			if(isset($d->int_pengaduan_respon_id)){ //Update Tanggapan //(3) Ditanggapi, (4) Selesai  : edit tanggapan & status selesai 
				//tanggapan & status
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_respon_id.'/tanggapan" class="ajax_modal btn btn-xs btn-success tooltips" data-placement="top" data-original-title="Tanggapan" style="width:120px"><i class="fas fa-comment-dots"></i> Tanggapan</a> ';	
			}else{ //(0) Draft, (1) Proses disposisi, (2) Ditolak : hapus dan disposisi
				//hapus
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" style="width:30px;margin-right:5px"><i class="fa fa-trash"></i></a>';
				//disposisi
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_pengaduan_id.'/get" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Verifikasi" style="width:85px"><i class="fa fa-share"></i> Disposisi</a> ';
			}

			$action .= '</div>';

			$data[] = array($i, $d->var_no_tiket, idn_date($d->dt_pengaduan, 'j F Y H:i:s'), $d->var_nama, $d->var_no_hp, $d->var_alamat, $type.'<br>'.$d->var_kategori, $d->var_topik, strip_tags($d->txt_note), strip_tags($d->txt_respon), $d->int_progress);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								'filter_start' => $filter_start,
								'filter_end' => $filter_end,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function export(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('tipe_filter', true), $this->input->post('kategori_filter', true), $this->input->post('progress_filter', true),$filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('tipe_filter', true), $this->input->post('kategori_filter', true), $this->input->post('progress_filter', true), $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $title    = 'Laporan Data Aspirasi';

        $filename = 'Laporan Data Aspirasi '.idn_date($filter_start, 'j F Y').' - '.idn_date($filter_end, 'j F Y').'.xlsx';


		$input_file = 'assets/export/xlsx/data_aspirasi.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);

		$sheet = $spreadsheet->getActiveSheet();

        $i = 0;
        $x = 1;
		foreach($ldata as $d){
			$x++;

			if($d->int_type == 1){
				$type = 'Pengaduan';
			}else if($d->int_type == 2){
				$type = 'Informasi';
			}else{
				$type = '';
			}

			switch ($d->int_progress) {
				case 0:
					$var_progress = 'Draft';
					break;
				case 1:
					$var_progress = 'Diproses';
					break;
				case 2:
					$var_progress = 'Ditolak';
					break;
				case 3:
					$var_progress = 'Ditanggapi';
					break;
				case 4:
					$var_progress = 'Selesai';
					break;
				default:
					$var_progress = 'Draft';
			  } 

			$sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValue('B'.$x, $d->var_no_tiket);
			$sheet->setCellValue('C'.$x, idn_date($d->dt_pengaduan, 'j F Y H:i:s'));
			$sheet->setCellValue('D'.$x, $d->var_nama);
			$sheet->setCellValue('E'.$x, $d->var_no_hp);
			$sheet->setCellValue('F'.$x, $d->var_alamat);
			$sheet->setCellValue('G'.$x, $type);
			$sheet->setCellValue('H'.$x, $d->var_kategori);
			$sheet->setCellValue('I'.$x, $d->var_topik);
			$sheet->setCellValue('J'.$x, strip_tags($d->txt_note));
			$sheet->setCellValue('K'.$x, strip_tags($d->txt_respon));
			$sheet->setCellValue('L'.$x, $var_progress);
		}

		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
