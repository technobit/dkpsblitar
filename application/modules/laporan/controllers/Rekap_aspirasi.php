<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
	
class Rekap_aspirasi extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'REKAP-ASPIRASI'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'laporan';
		$this->routeURL = 'rekap_aspirasi';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('rekap_aspirasi_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Rekapitulasi Data Aspirasi';
		$this->page->menu = 'laporan';
		$this->page->submenu1 = 'rekap_aspirasi';
		$this->breadcrumb->title = 'Data Aspirasi Masyarakat';
		$this->breadcrumb->card_title = 'Data Aspirasi Masyarakat';
		$this->breadcrumb->icon = 'fas fa-file-alt';
		$this->breadcrumb->list = ['rekap', 'Data Aspirasi Masyarakat'];
		$this->js = true;
		$this->css = true;
		$data['kategori_list'] = $this->kategori->get_list('int_pengaduan');
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('rekap_aspirasi/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$ldata = $this->model->list($filter_start, $filter_end, $this->input_post('tipe_filter', TRUE));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;

			$data[] = array($i, $d->var_kategori, $d->draft, $d->disetujui, $d->ditolak, $d->ditanggapi, $d->selesai, $d->jumlah);
		}
		$this->set_json(array( 'stat' => TRUE,
								'aaData' => $data,
								'filter_start' => $filter_start,
								'filter_end' => $filter_end,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function export(){
		$this->authCheckDetailAccess('r');
		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$ldata = $this->model->list($filter_start, $filter_end, $this->input_post('tipe_filter', TRUE));

        $title    = 'Rekap Data Aspirasi';

        $filename = 'Rekap Data Aspirasi '.idn_date($filter_start, 'j F Y').' - '.idn_date($filter_end, 'j F Y').'.xlsx';


		$input_file = 'assets/export/xlsx/rekap_aspirasi.xlsx';
		/** Load $inputFileName to a Spreadsheet object **/
		//$spreadsheet = new Spreadsheet();
		$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($input_file);

		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('D1', $this->input_post('tipe', TRUE));
		$sheet->setCellValue('D2', idn_date($filter_start, 'j F Y'));
		$sheet->setCellValue('D3', idn_date($filter_end, 'j F Y'));

        $i = 0;
        $x = 5;
		foreach($ldata as $d){
			$x++;
			$sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValue('B'.$x, $d->var_kategori);
			$sheet->setCellValue('C'.$x, $d->draft);
			$sheet->setCellValue('D'.$x, $d->disetujui);
			$sheet->setCellValue('E'.$x, $d->ditolak);
			$sheet->setCellValue('F'.$x, $d->ditanggapi);
			$sheet->setCellValue('G'.$x, $d->selesai);
			$sheet->setCellValue('H'.$x, $d->jumlah);
		}

		$sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
	}
}
