<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Laporan_aspirasi_model extends MY_Model {

    public function list($tipe_filter = "", $kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$this->db->select("tp.*, tt.int_pengaduan_respon_id, tt.dt_respon, tt.txt_respon")
					->from($this->t_pengaduan." tp")
					->join($this->t_pengaduan_respon." tt", "tp.int_pengaduan_id = tt.int_pengaduan_id", "left")
					->where('tp.int_status = 1');

		/*if($this->session->userdata['group_id'] !== 1 || $this->session->userdata['group_id'] !== 2){
			$this->db->where('tp.int_user_id', $this->session->userdata['user_id']);
		}*/

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('tp.dt_pengaduan', $filter_start, $filter_end);
		}

		if(($tipe_filter!="")){ // filter
            $this->db->where('tp.int_type', $tipe_filter);
		}
		
		if(($kategori_filter!="")){ // filter
            $this->db->where('tp.int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('tp.int_progress', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tp.var_no_tiket', $filter)
					->or_like('tp.var_topik', $filter)
					->or_like('tp.txt_note', $filter)
					->group_end();
		}

		$order = 'dt_pengaduan';
		switch($order_by){
			case 1 : $order = 'tp.dt_pengaduan '; break;
			case 2 : $order = 'tp.var_topik '; break;
			default: {$order = 'tp.dt_pengaduan '; $sort = 'DESC';} break;
		}
		
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($tipe_filter = "", $kategori_filter = "",  $progress_filter = "", $filter_start = null, $filter_end = null, $filter = NULL){
		$this->db->select("tp.*, tt.int_pengaduan_respon_id")
					->from($this->t_pengaduan." tp")
					->join($this->t_pengaduan_respon." tt", "tp.int_pengaduan_id = tt.int_pengaduan_id", "left")
					->where('tp.int_status = 1 ');

		/*if($this->session->userdata['group_id'] == 1){
			$this->db->where('tp.int_user_id', $this->session->userdata['user_id']);
		}*/

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('tp.dt_pengaduan', $filter_start, $filter_end);
		}

		if(($tipe_filter!="")){ // filter
            $this->db->where('tp.int_type', $tipe_filter);
		}

		if(($kategori_filter!="")){ // filter
            $this->db->where('tp.int_kategori_id', $kategori_filter);
		}

		if($progress_filter!=""){ // filter
            $this->db->where('tp.int_progress', $progress_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tp.var_no_tiket', $filter)
					->or_like('tp.var_topik', $filter)
					->or_like('tp.txt_note', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}	
}
