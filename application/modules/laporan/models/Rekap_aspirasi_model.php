<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Rekap_aspirasi_model extends MY_Model {

    public function list($filter_start = "", $filter_end = "", $filter_type = 0){
        return $this->callProcedure("rpt_rekap_by_status(?,?,?)", [$filter_start, $filter_end, $filter_type], 'result');
    }
}
