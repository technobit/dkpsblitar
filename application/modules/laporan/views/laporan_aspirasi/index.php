<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-sm btn-success" onclick="exportData(this)" data-block="body" data-url="<?php echo $url_export ?>" ><i class="fas fa-download"></i> Export Excel <i class="fas fa-file-excel"></i></button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
                            <div class="col-md-3">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="date_filter" class="col-md-3 col-form-label">Date</label>
									<div class="col-md-9">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter">
									</div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row text-sm mb-0">
                                    <label for="tipe_filter" class="col-sm-4 col-form-label">Tipe Aspirasi</label>
                                    <div class="col-sm-8">
                                        <select id="tipe_filter" name="tipe_filter" class="form-control form-control-sm select2 tipe_filter" style="width:100%">
                                            <option value="">- Semua Aspirasi -</option>
                                            <option value="1">Pengaduan</option>
                                            <option value="2">Informasi</option>
                                        </select>
                                    </div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row text-sm mb-0">
                                    <label for="kategori_filter" class="col-sm-4 col-form-label">Kategori</label>
                                    <div class="col-sm-8">
                                        <select id="kategori_filter" name="kategori_filter" class="form-control form-control-sm select2 kategori_filter" style="width:100%">
                                            <option value="">- Semua Kategori -</option>
                                            <?php 
                                                foreach($kategori_list as $kat){
                                                    echo '<option value="'.$kat->int_kategori_id.'">'.$kat->var_kategori.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row text-sm mb-0">
                                    <label for="progress_filter" class="col-sm-4 col-form-label">Progress</label>
                                    <div class="col-sm-8">
                                        <select id="progress_filter" name="progress_filter" class="form-control form-control-sm select2 progress_filter" style="width:100%">
                                            <option value="">- Semua Status -</option>
                                            <option value="0">Draft</option>
                                            <option value="1">Diproses</option>
                                            <option value="2">Ditolak</option>
                                        </select>
                                    </div>
								</div>
							</div>
						</div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="data_table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>No. Tiket</th>
                            <th>Tanggal</th>
                            <th>Nama</th>
                            <th>No. Telp</th>
                            <th>Alamat</th>
                            <th>Kategori</th>
                            <th>Topik/Judul</th>
                            <th>Detil Aspirasi</th>
                            <th>Tanggapan</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
