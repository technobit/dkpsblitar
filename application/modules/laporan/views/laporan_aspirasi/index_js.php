<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $('.kategori_filter').select2();
    $('.progress_filter').select2();
    $('.tipe_filter').select2();

    var dataTable;

    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                date_filter : $('.date_filter').val(),
                kategori_filter : $('.kategori_filter').val(),
                progress_filter : $('.progress_filter').val(),
                tipe_filter : $('.tipe_filter').val()
            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }
    
    $(document).ready(function() {
        dataTable = $('#data_table').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            //"order": [[ 1, "desc" ]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.kategori_filter = $('.kategori_filter').val();
					d.progress_filter = $('.progress_filter').val();
					d.tipe_filter = $('.tipe_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "15", //nomor
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "100", //no tiket
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "150", //tanggal
                    "sClass": "text-right vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //nama
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //tipe
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto", //kategori
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto",//topik
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "auto", 
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "auto", 
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [10],//post type
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span class="badge bg-black">Draft</span>';
                                break;
                            case 1:
                                return '<span class="badge bg-warning">Diproses</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-danger">Ditolak</span>';
                                break;
                            case 3:
                                return '<span class="badge bg-success">Ditanggapi</span>';
                                break;
                            case 4:
                                return '<span class="badge bg-primary">Selesai</span>';
                                break;
                            default:
                                return '<span class="badge bg-black">Draft</span>';
                                break;
                        }
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
		});

        $('.kategori_filter').change(function(){
            dataTable.draw();
        });
		
        $('.progress_filter').change(function(){
            dataTable.draw();
        });

        $('.tipe_filter').change(function(){
            dataTable.draw();
        });
    });
</script>