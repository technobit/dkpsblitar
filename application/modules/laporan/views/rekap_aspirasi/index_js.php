<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $('.tipe_filter').select2();

    var dataTable;

    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?php echo $page->tokenName ?> : $('meta[name=<?php echo $page->tokenName ?>]').attr("content"),
                date_filter : $('.date_filter').val(),
                tipe_filter : $('.tipe_filter').val(),
                tipe : $(".tipe_filter option:selected").html()

            }
        });
        setTimeout(function(){unblockUI(blc)}, 5000);
    }
    
    $(document).ready(function() {
        dataTable = $('#data_table').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "searching": false,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.tipe_filter = $('.tipe_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "15", //nomor
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-left vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
		});

        $('.tipe_filter').change(function(){
            dataTable.draw();
        });
    });
</script>