<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['m_kategori']['get']          			    = 'master/kategori';
$route['m_kategori']['post']         			    = 'master/kategori/list';
$route['m_kategori/add']['get']      			    = 'master/kategori/add';
$route['m_kategori/save']['post']    			    = 'master/kategori/save';
$route['m_kategori/([a-zA-Z0-9]+)']['get']          = 'master/kategori/get/$1';
$route['m_kategori/([a-zA-Z0-9]+)']['post']     	= 'master/kategori/update/$1';
$route['m_kategori/([a-zA-Z0-9]+)/del']['get']      = 'master/kategori/confirm/$1';
$route['m_kategori/([a-zA-Z0-9]+)/del']['post'] 	= 'master/kategori/delete/$1';

$route['m_bidang']['get']                           = 'master/bidang';
$route['m_bidang']['post']         			        = 'master/bidang/list';
$route['m_bidang/add']['get']      			        = 'master/bidang/add';
$route['m_bidang/save']['post']    			        = 'master/bidang/save';
$route['m_bidang/([a-zA-Z0-9]+)']['get']            = 'master/bidang/get/$1';
$route['m_bidang/([a-zA-Z0-9]+)']['post']     	    = 'master/bidang/update/$1';
$route['m_bidang/([a-zA-Z0-9]+)/del']['get']        = 'master/bidang/confirm/$1';
$route['m_bidang/([a-zA-Z0-9]+)/del']['post'] 	    = 'master/bidang/delete/$1';

$route['wilayah/kelurahan']['post']                 = 'master/wilayah/get_kelurahan';

$route['m_banner']['get']       = 'master/banner';
$route['m_banner']['post']      = 'master/banner/list';
$route['m_banner/save']['post'] = 'master/banner/save';
$route['m_banner/upd']['get']   = 'master/banner/form';
