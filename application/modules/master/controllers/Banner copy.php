<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'BANNER';
        $this->module   = 'master';
        $this->routeURL = 'm_banner';
		$this->authCheck();

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;

		$this->load->model('banner_model', 'model');
    }
	
	public function index(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $this->breadcrumb->title = 'Banner Homepage';
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        $data['banner']         = $this->model->get_banner();
        $data['url_update']     = site_url("{$this->routeURL}");
		$this->render_view('banner/index', $data, false);
	}

    public function update(){
        $this->authCheckDetailAccess('u'); // hak akses untuk render page

        $this->form_validation->set_rules('int_banner_id', 'Banner ID', 'required');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('stat', false);
            $this->session->set_flashdata('msg', 'Data profile gagal disimpan. '.validation_errors('<br>', ' '));
        } else {
            if(isset($_FILES["image"]["name"])){
                $year = date("Y");
                $month = date("m");
                $img_dir = $this->config->item('img_dir').$year."/".$month; 
                $upload_path = $this->config->item('upload_path').$year."/".$month; 
                if (!file_exists($upload_path)) {
                    mkdir($upload_path, 0777, true);
                }
                $config['upload_path']   = $upload_path;
                $config['allowed_types'] = $this->config->item('allowed_types'); 
                $config['encrypt_name']  = $this->config->item('encrypt_name');
                $config['max_size']      = $this->config->item('max_size');
                $this->load->library('upload', $config);
                if($this->upload->do_upload('image')){
                    $data = $this->upload->data();
                    $file_name = cdn_url().$img_dir.'/'.$data['file_name'];
                    $stat = TRUE;
                }else{
                    $file_name = $this->upload->display_errors();
                    $stat = FALSE;
                }
            }

            $this->model->update($this->input_post());

            $this->session->set_flashdata('stat', true);
            $this->session->set_flashdata('msg', 'Data profile berhasil disimpan.');
        }

        redirect('m_banner');
    }
}
