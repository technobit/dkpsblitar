<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Banner extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'BANNER'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'master';
		$this->routeURL = 'm_banner';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('banner_model', 'model');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Homepage Banner';
		$this->page->menu = 'master';
		$this->page->submenu1 = 'm_banner';
		$this->breadcrumb->title = 'Homepage Banner';
		$this->breadcrumb->card_title = 'Homepage Banner';
		$this->breadcrumb->icon = 'fas fa-image';
		$this->breadcrumb->list = ['Data Induk', 'Banner'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/upd");
		$this->render_view('banner/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount();
		$ldata = $this->model->list();

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array('<img src="'.cdn_url().$d->var_img_banner.'" alt="Homepage Banner" width="100%" height="auto">');
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function form(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Ubah Baneer';
		$this->load_view('banner/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('int_banner_id', 'Banner ID', 'required');
	    
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$upload_msg 	= [];
			$files 			= [];
			$img_up_stat	= true;
			$data_post		= $this->input->post();
			if(isset($data_post['cdImg'])){
				$cdImg = $data_post['cdImg'];
			}

			if(isset($_FILES['input_foto_banner'])){
				$upload_banner_image = $this->upload_banner_image($_FILES['input_foto_banner'], 'input_foto_banner', $cdImg['banner']);
				if(isset($upload_banner_image['var_image'])){
					$data_post['var_img_banner'] = $upload_banner_image['var_image'];
				}else{
					$img_up_stat = false;
				}
				$upload_msg[$cdImg['banner']] = $upload_banner_image['upload_msg'];
			}
			$files = ['files' => $upload_msg];
			if($img_up_stat==true){
				$check = $this->model->save($data_post);
				if($check){
					$json_status = true;
					$json_mc = true;
					$json_msg = "Data Berhasil Disimpan";	
				}else{
					$json_status = false;
					$json_mc = false;
					$json_msg = "Data Gagal Disimpann";
				}
			}else{
					$json_status = false;
					$json_mc = false;
					$json_msg = "Data Gagal Disimpans";
			}
			$this->set_json(array_merge([ 'stat' => $json_status, 
								'mc' => $json_mc,
								'msg' => $json_msg,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
        }
	}	

	public function upload_banner_image($input_file, $input_file_name, $cdImg){
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}

		$config['upload_path']   = $upload_path;
		$config['allowed_types'] = $this->config->item('allowed_types'); 
		$config['encrypt_name']  = $this->config->item('encrypt_name');
		$config['max_size']      = $this->config->item('max_size');

		$this->load->library('upload', $config);
		
		if($this->upload->do_upload($input_file_name)){
			$file_name = $this->upload->data('file_name');
			$ret['var_image'] = $img_dir.'/'.$this->upload->data('file_name');
			$ret['upload_msg'] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg];
			//$ret['img_up_stat'] = true;
		}else{
			$ret['upload_msg'] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg];
			$ret['img_up_stat'] = false;
		}
		$this->upload->error_msg = [];

		return $ret;
	}
}
