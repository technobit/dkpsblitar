<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Banner_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_banner_id, var_banner 
								 FROM	{$this->m_banner}
								 WHERE int_status = 1
								 ORDER BY int_banner_id ASC")->result();
	}

    public function list(){
		$this->db->select("*")
					->from($this->m_banner)
					->where('int_banner_id', 1);

		return $this->db->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->select("*")
					->from($this->m_banner)
					->where('int_banner_id', 1);
                    
		return $this->db->count_all_results();
	}

    public function save($upd){
	    /*$this->db->where('int_banner_id', 1)
                    ->update($this->m_banner, ['var_img_banner' => $upd['var_img_banner']]);
        */
        $this->db->trans_begin();
		$upd = $this->clearFormInsert($upd, ['cdImg', 'ci_csrf_token']);

        $this->db->where('int_banner_id', 1);
        $this->db->update($this->m_banner, $upd);

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return false;
        }else{
            $this->db->trans_commit();
            return true;
                    }
    }	
}
