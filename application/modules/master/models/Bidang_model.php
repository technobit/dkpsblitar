<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Bidang_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_bidang_id, var_bidang 
								 FROM	{$this->m_bidang}
								 WHERE int_status = 1
								 ORDER BY int_bidang_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_bidang)
					->where('int_status', 1);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_bidang', $filter)
					->group_end();
		}

		$order = 'var_bidang ';
		switch($order_by){
			case 1 : $order = 'var_bidang '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_bidang)
				->where('int_status', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_bidang', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$upd['created_at'] = date("Y-m-d H:i:s");
		$upd['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_bidang, $ins);
	}

	public function get($int_bidang_id){
		return $this->db->select("*")
					->get_where($this->m_bidang, ['int_bidang_id' => $int_bidang_id])->row();
	}

	public function update($int_bidang_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_bidang_id', $int_bidang_id);
		$this->db->update($this->m_bidang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_bidang_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_bidang_id', $int_bidang_id);
		$this->db->update($this->m_bidang, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
