<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Kategori_model extends MY_Model {

	public function get_list($mode = ''){
		if($mode != ''){
			$mode_query = 'AND '.$mode.' = 1';
		}else{
			$mode_query = '';
		}

		return $this->db->query("SELECT int_kategori_id, var_kategori 
								 FROM	{$this->m_kategori}
								 WHERE int_status = 1
								 {$mode_query}
								 ORDER BY int_kategori_id ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_kategori)
					->where('int_status', 1);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('var_kategori', $filter)
					->group_end();
		}

		$order = 'var_kategori ';
		switch($order_by){
			case 1 : $order = 'var_kategori '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_kategori)
				->where('int_status', 1);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_kategori', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		$ins['int_status'] = 1;
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		return $this->db->insert($this->m_kategori, $ins);
	}

	public function get($int_kategori_id){
		return $this->db->select("*")
					->get_where($this->m_kategori, ['int_kategori_id' => $int_kategori_id])->row();
	}

	public function update($int_kategori_id, $upd){
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();

		$this->db->where('int_kategori_id', $int_kategori_id);
		$this->db->update($this->m_kategori, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_kategori_id){
		$upd['deleted_at'] = date("Y-m-d H:i:s");
		$upd['deleted_by'] = $this->session->userdata['user_id'];
		$upd['int_status'] = 0;
		$this->db->trans_begin();

		$this->db->where('int_kategori_id', $int_kategori_id);
		$this->db->update($this->m_kategori, $upd);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	
}
