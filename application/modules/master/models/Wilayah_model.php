<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Wilayah_model extends MY_Model {

	public function get_kelurahan(){
		return $this->db->query("SELECT *
				FROM	{$this->m_kelurahan}
				ORDER BY int_kelurahan_id")->result();
	}
}
