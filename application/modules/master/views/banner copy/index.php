<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="fas fa-user mr-1"></i>
                        Banner Homepage 
                    </h3>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <?php
                            echo form_open($url_update, ['method' => "POST", "role" => "form", "class" => "form-horizontal", "id" =>"banner-form"]);
                            $msg = $this->session->flashdata('msg');
                            if(!empty($msg)){
                                echo '<div class="alert alert-'.(($this->session->flashdata('stat'))? 'success' : 'danger').'">'.$msg.'</div>';
                            }
                        ?>
                            <div class="form-group row mb-1">
                                <label for="image" class="col-sm-3 col-form-label">Banner</label>
                                <div class="col-sm-9">
                                    <input type="file" name="image" id="image" accept="image/png, image/jpeg, image/jpg, image/gif">
                                </div>
                            </div>
                            <input type="text" class="form-control" id="int_banner_id" name="int_banner_id" value="<?php echo $banner->int_banner_id?>" hidden/>
                            <div class="form-group row mb-1">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>