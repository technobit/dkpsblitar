<script>
    $(document).ready(function(){
        $("#banner-form").validate({
            rules: {
                image: {
                    required: true
                }
            },
            validClass: "valid-feedback",
            errorElement: "div", // contain the error msg in a small tag
            errorClass: 'invalid-feedback',
            errorPlacement: erp,
            highlight: hl,
            unhighlight: uhl,
            success: sc
        });
    });
</script>