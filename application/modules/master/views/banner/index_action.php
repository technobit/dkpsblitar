<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action" width="100%">
<div id="modal-form" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">UBAH BANNER HOMEPAGE</h3>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12">
								<div class="input-group custom-file">
									<input type="file" class="custom-file-input input_foto" name="input_foto_banner" id="input_foto_banner" accept="image/*">
									<div class="input-group-prepend">
										<span class="custom-file-label" for="input_foto">Pilih Gambar</span>
									</div>
								</div>
								<label> <i class="required">*) Format gambar yang diijinkan adalah png, jpg, dan jpeg</i></label>
								<div id="view_foto_banner" class="img-upload-area">
								</div>
							</div>
							<input type="text" class="form-control" id="int_banner_id" name="int_banner_id" value="1" hidden/>

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success" id="save_button">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>	
	$(document).ready(function(){
		$(".img-popup").lightGallery();

		$("#action").validate({
			rules: {
			    int_banner_id:{required: true }
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#action');
							dataTable.draw();
						}
						closeModal($modal, data);
						if(data.hasOwnProperty('files')){
							$.each(data.files, function(i, v){
								if(v.stat){
									if(!$('.'+v.cdImg).hasClass('is-valid')) $('.title-image-'+i).addClass('is-valid');
									if(!$('.'+v.cdImg).next('div').hasClass('valid-feedback'))$('.title-image-'+i).after('<div id="'+i+'-valid" class="valid-feedback">'+v.msg+'</div>');
								}else{
									if(!$('.'+v.cdImg).hasClass('is-invalid')) $('.'+v.cdImg).addClass('is-invalid');
									if(!$('.'+v.cdImg).next('div').hasClass('invalid-feedback'))$('.'+v.cdImg).after('<div id="'+i+'-error" class="invalid-feedback">'+v.msg+'</div>');
								}
								
								
							});
						}
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});

		bsCustomFileInput.init();
		$( ".preview-images-zone" ).sortable();
		$(document).on('click', '.image-cancel', function() {
			let no = $(this).data('no');
			//$(".preview-image.preview-show-"+no).parent().parent().remove();
			$(this).parent().parent().remove();
		});

		$('#input_foto_banner').change(function(){
			preview_img_upload('banner', 'lg');
		});
		
		function preview_img_upload(preview_id, preview_size){
			if (window.File && window.FileList && window.FileReader) {
				var files = event.target.files; //FileList object
				img_preview_size = 'style="height:auto;max-height:auto;width:auto;max-width:100%"';
				for (let i = 0; i < 1; i++) {
					var file = files[0];
					if (!file.type.match('image')) continue;
					
					var picReader = new FileReader();
					let cdImg 	= 'img'+getSandStr(3,'-',3);
					
					picReader.addEventListener('load', function (event) {
						var picFile = event.target;
						var html =  '<div class="img-client"><input type="hidden" name="cdImg['+preview_id+']" value="'+cdImg+'"/>' +
									'<div class="preview-image preview-show-'+preview_id+'">' +
									'<div class="image-zone"><img  '+img_preview_size+' id="pro-img-'+preview_id+'" src="'+picFile.result+'"></div>' +
									'</div><input type="hidden" class="form-control form-control-sm '+cdImg+'" disabled/></div>';

						/*var html =  '<div class="preview-image preview-show-'+preview_id+'">' +
									'<div class="image-zone"><img style="width:100%" id="pro-img-'+preview_id+'" src="'+picFile.result+'"></div></div>';*/
						document.getElementById('view_foto_'+preview_id+'').innerHTML = html;
					});

					picReader.readAsDataURL(file);
				}
			}else {
				alert('Browser not support');
			}
		}
	});
</script>