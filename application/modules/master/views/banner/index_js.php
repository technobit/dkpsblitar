<script src="<?=base_url() ?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>

<script>
    var dataTable;
    $(document).ready(function() {

		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "bFilter": false,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": false,
            "paging" : false,
            "info" : false,
            "ordering" : false,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ]
        });
    });
</script>