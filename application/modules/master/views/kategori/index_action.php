<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="data_form" width="80%">
<div id="modal-tag" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="var_kategori" class="col-sm-4 col-form-label">Kategori <i class="required">*</i></label>
				<div class="col-sm-8">
					<input type="text" class="form-control form-control-sm" id="var_kategori" placeholder="Kategori" name="var_kategori" value="<?=isset($data->var_kategori)? $data->var_kategori : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-4 col-form-label">Pengaduan</label>
				<div class="col-sm-8 mt-1">
					<div class="icheck-success d-inline mr-2">
						<input type="radio" id="int_pengaduan1" name="int_pengaduan" value="1" <?=isset($data->int_pengaduan)? (($data->int_pengaduan == 1)? 'checked' : '') : 'checked' ?>>
							<label for="int_pengaduan1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="int_pengaduan2" name="int_pengaduan" value="0" <?=isset($data->int_pengaduan)? (($data->int_pengaduan == 0)? 'checked' : '') : '' ?>>
						<label for="int_pengaduan2">Tidak Aktif</label>
					</div>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-4 col-form-label">Informasi</label>
				<div class="col-sm-8 mt-1">
					<div class="icheck-success d-inline mr-2">
						<input type="radio" id="int_informasi1" name="int_informasi" value="1" <?=isset($data->int_informasi)? (($data->int_informasi == 1)? 'checked' : '') : 'checked' ?>>
							<label for="int_informasi1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="int_informasi2" name="int_informasi" value="0" <?=isset($data->int_informasi)? (($data->int_informasi == 0)? 'checked' : '') : '' ?>>
						<label for="int_informasi2">Tidak Aktif</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){

		$("#data_form").validate({
			rules: {
			    var_kategori:{
			        required: true,
					minlength: 2,
					maxlength: 200
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-tag';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#data_form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>