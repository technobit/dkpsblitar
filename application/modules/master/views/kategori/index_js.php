<script>
    var dataTable;
    $(document).ready(function() {
        dataTable = $('#table_data').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 25,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "20",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "50",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [2],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span style="font-size:21px;color:#dc3545"><i class="fa fa-minus-circle"></i></span>';
                                break;
                            case 1:
                                return '<span style="font-size:21px;color:#28a745"><i class="fa fa-check-circle"></i></span>';
                                break;
                            default:
                                return '<span style="font-size:21px;color:#dc3545"><i class="fa fa-minus-circle"></i></span>';
                                break;
                        }
                    }
                },
                {
                    "aTargets": [3],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span style="font-size:21px;color:#dc3545"><i class="fa fa-minus-circle"></i></span>';
                                break;
                            case 1:
                                return '<span style="font-size:21px;color:#28a745"><i class="fa fa-check-circle"></i></span>';
                                break;
                            default:
                                return '<span style="font-size:21px;color:#dc3545"><i class="fa fa-minus-circle"></i></span>';
                                break;
                        }
                    }
                },
                {
                    "aTargets": [4],
                    "mRender": function(data, type, row, meta) {
                        return  ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit data" ><i class="fa fa-edit"></i></a> ') +
                                ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus data" ><i class="fa fa-trash"></i></a> ');
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>