<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class User_model extends MY_Model {

	public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("u.int_user_id as user_id, u.var_username as username, CONCAT(u.var_nama_depan, ' ', u.var_nama_belakang) as nama, g.var_nama as grup, u.int_status as is_aktif, int_bidang_id")
					->from($this->s_user. ' u')
					->join($this->s_group. ' g', 'g.int_group_id = u.int_group_id');

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('u.var_username', $filter)
					->or_like('u.var_nama_depan', $filter)
					->or_like('u.var_nama_belakang', $filter)
					->group_end();
		}

		$order = 'u.var_username ';
		switch($order_by){
			case 1 : $order = 'u.var_username '; break;
			case 2 : $order = 'u.var_nama_depan '; break;
			case 3 : $order = 'g.var_nama '; break;
			case 4 : $order = 'u.int_status '; break;
			default : $order = 'u.int_user_id '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->s_user. ' u')
                 ->join($this->s_group. ' g', 'g.int_group_id = u.int_group_id');

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
                    ->like('u.var_username', $filter)
                    ->or_like('u.var_nama_depan', $filter)
                    ->or_like('u.var_nama_belakang', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function create($in){
		$col['int_group_id']		= $in['group_id'];
        $col['var_nama_depan']		= $in['first_name'];
        $col['var_nama_belakang']	= $in['last_name'];
        $col['var_username']		= $in['username'];
        $col['var_password']		= password_hash($in['password'], PASSWORD_BCRYPT);
        $col['int_bidang_id']		= $in['int_bidang_id'];
        $col['int_status']			= $in['is_aktif'];

        $this->db->trans_begin();
		$this->db->insert($this->s_user, $col);
		$user_id = $this->db->insert_id();
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get($user_id){
		$data =  $this->db->query("	SELECT u.int_user_id as user_id, u.var_username as username, u.var_nama_depan as first_name, u.var_nama_belakang as last_name, u.int_group_id as group_id, u.int_status as is_aktif, int_bidang_id
									FROM	{$this->s_user} u  
									WHERE	u.int_user_id = ?", [$user_id])->row_array();

		return (object) $data;									
	}

	public function update($user_id, $in){
		if(!empty($in['password'])){
			$col['var_password'] = password_hash($in['password'],PASSWORD_BCRYPT);
		}
        $col['int_group_id']		= $in['group_id'];
        $col['var_nama_depan']		= $in['first_name'];
        $col['var_nama_belakang']	= $in['last_name'];
        $col['var_username']		= $in['username'];
        $col['int_bidang_id']		= $in['int_bidang_id'];
        $col['int_status']			= $in['is_aktif'];
		$this->db->trans_begin();

		$this->db->where('int_user_id', $user_id);
		$this->db->update($this->s_user, $col);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($user_id){
	    if($user_id < 2) return false;

		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_user} WHERE int_user_id = ?", [$user_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
