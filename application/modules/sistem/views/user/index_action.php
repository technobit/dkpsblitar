<?php
    $readOnly = isset($data->user_id)? (($data->user_id > 1)? '' : 'readonly') : '';
?>
<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="user-form" width="80%">
<div id="modal-user" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">Group</label>
				<div class="col-sm-10">
					<select <?=$readOnly ?> id="group" name="group_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih -</option>
						<?php 
							foreach($group as $g){
								echo '<option value="'.$g->group_id.'">'.$g->nama.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="username" class="col-sm-2 col-form-label">Username</label>
				<div class="col-sm-10">
					<input type="text" class="form-control form-control-sm" id="username" placeholder="Username" name="username" value="<?=isset($data->username)? $data->username : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="nama" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-5">
					<input type="text" class="form-control form-control-sm" id="first_name" placeholder="Nama Depan" name="first_name" value="<?=isset($data->first_name)? $data->first_name : ''?>"/>
				</div>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm" id="last_name" placeholder="Nama Belakang" name="last_name" value="<?=isset($data->last_name)? $data->last_name : ''?>"/>
                </div>
			</div>
			<div class="form-group row mb-1">
				<label for="Password" class="col-sm-2 col-form-label">Passwords</label>
				<div class="col-sm-10">
					<input type="password" class="form-control form-control-sm" id="password" placeholder="Password" name="password" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="int_bidang_id" class="col-sm-2 col-form-label">Bidang</label>
				<div class="col-sm-10">
					<select id="int_bidang_id" name="int_bidang_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="0">- Pilih Bidang-</option>
						<?php 
							foreach($bidang as $bd){
								echo '<option value="'.$bd->int_bidang_id.'">'.$bd->var_bidang.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10 mt-1">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="is_aktif" value="1" <?=isset($data->is_aktif)? (($data->is_aktif == 1)? 'checked' : '') : 'checked' ?>>
							<label for="radioPrimary1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="radioPrimary2" name="is_aktif" value="0" <?=isset($data->is_aktif)? (($data->is_aktif == 0)? 'checked' : '') : '' ?>>
						<label for="radioPrimary2">Non-aktif</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		<?php if(isset($data->group_id)) echo '$("#group").val("'.$data->group_id.'").trigger("change");'?>
		<?php if(isset($data->int_bidang_id)) echo '$("#int_bidang_id").val("'.$data->int_bidang_id.'").trigger("change");'?>

		$("#user-form").validate({
			rules: {
				group_id: {
					required: true
				},
				username: {
					required: true,
					minlength: 3,
					maxlength: 20
				},
				first_name: {
					required: true,
					minlength: 4,
					maxlength: 50
				},
                last_name: {
                    minlength: 4,
                    maxlength: 50
                },
				password: {
					<?=isset($data)? '' : 'required: true,' ?>
					minlength: 4,
					maxlength: 20
				},
				is_aktif: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#user-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>