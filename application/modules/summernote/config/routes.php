<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['summernote/upload_image']['post']	= 'summernote/image/upload';
$route['summernote/delete_image']['post']	= 'summernote/image/delete';