<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends MX_Controller {
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'SUMMERNOTE';
		//$this->authCheck();
		
		//$this->load->model('app_model', 'model');
    }
	
    //Upload image summernote
 
    function upload(){
		
		if(isset($_FILES["image"]["name"])){
            $year = date("Y");
            $month = date("m");
            $img_dir = $this->config->item('img_dir').$year."/".$month; 
            $upload_path = $this->config->item('upload_path').$year."/".$month; 
            if (!file_exists($upload_path)) {
                mkdir($upload_path, 0777, true);
            }
            $config['upload_path']   = $upload_path;
            $config['allowed_types'] = $this->config->item('allowed_types'); 
            $config['encrypt_name']  = $this->config->item('encrypt_name');
            $config['max_size']      = $this->config->item('max_size');
            $this->load->library('upload', $config);
			if($this->upload->do_upload('image')){
                $data = $this->upload->data();
                $file_name = cdn_url().$img_dir.'/'.$data['file_name'];
                $stat = TRUE;
            }else{
                $file_name = $this->upload->display_errors();
                $stat = FALSE;
            }
		}
		$this->set_json(array( 'stat' => $stat, 
								'data' => $file_name));
    }
    
    //Delete image summernote
    function delete(){
        $src = $this->input->post('src'); // http://localhost/dkpsblitar/assets/images/2021/06/91f1fdb34282d0ff245affa7399f08d4.PNG
        $img_url = cdn_url().'assets/images/'; // http://localhost/dkpsblitar/assets/images/
        $img_url = str_replace($img_url,"",$src); // 2021/06/91f1fdb34282d0ff245affa7399f08d4.PNG
        $upload_path = $this->config->item('upload_path'); // assets/images/
        $unlink = unlink($upload_path.$img_url); // assets/images/assets/images/2021/06/91f1fdb34282d0ff245affa7399f08d4.PNG

        $stat = FALSE;
        $data =  'Failed Delete File';
        if($unlink){
            $stat = TRUE;
            $data =  'File Delete Successfully';
        }
		$this->set_json(array( 'stat' => $stat, 
								'data' => $data));    
    }
}
