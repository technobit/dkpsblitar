<div class="col-lg-4">
	<aside class="widget-area">
		<!-- widget single item start -->
		<div class="card widget-item">
			<h4 class="widget-title">Pengaduan Terbaru</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?php foreach ($pengaduan as $adu){?>
					<li class="unorder-list">
						<div class="unorder-list-info">
							<h3 class="list-title">
							<a href="<?=base_url('adu/det/')?><?=$adu->int_pengaduan_id?>">
							<?=$adu->var_topik?></a>
							</h3>
							<p class="list-subtitle"><?=idn_date($adu->created_at, 'l, j F Y H:i:s')?></p>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div class="card widget-item">
			<h4 class="widget-title">Informasi Terbaru</h4>
			<div class="widget-body">
				<ul class="like-page-list-wrapper">
					<?php foreach ($informasi as $info){?>
					<li class="unorder-list">
						<div class="unorder-list-info">
							<h3 class="list-title">
							<a href="<?=base_url('info/det/')?><?=$info->int_pengaduan_id?>">
							<?=$info->var_topik?></a>
							</h3>
							<p class="list-subtitle"><?=idn_date($info->created_at, 'l, j F Y H:i:s')?></p>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<!-- widget single item end -->

	</aside>
</div>